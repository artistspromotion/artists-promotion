<html>
<head>
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    

<style>
html,body{margin:0}
 ul.slide{margin:0;
          padding:0;
          height:80px;
          list-style-type:none;}
 ul.slide li{float:left;
             list-style-type:none;}
 ul.slide img{border:1px solid silver;
             height:80px;}


</style>
<script>
 //Plugin start
 (function($)
   {
     var methods = 
       {
         init : function( options ) 
         {
           return this.each(function()
             {
               var _this=$(this);
                   _this.data('marquee',options);
               var _li=$('>li',_this);
                   
                   _this.wrap('<div class="slide_container"></div>')
                        .height(_this.height())
                       .hover(function(){if($(this).data('marquee').stop){$(this).stop(true,false);}},
                              function(){if($(this).data('marquee').stop){$(this).marquee('slide');}})
                        .parent()
                        .css({position:'relative',overflow:'hidden','height':$('>li',_this).height()})
                        .find('>ul')
                        .css({width:screen.width*2,position:'absolute'});
           
                   for(var i=0;i<Math.ceil((screen.width*3)/_this.width());++i)
                   {
                     _this.append(_li.clone());
                   } 
             
               _this.marquee('slide');});
         },
      
         slide:function()
         {
           var $this=this;
           $this.animate({'left':$('>li',$this).width()*-1},
                         $this.data('marquee').duration,
                         'swing',
                         function()
                         {
                           $this.css('left',0).append($('>li:first',$this));
                           $this.delay($this.data('marquee').delay).marquee('slide');
             
                         }
                        );
                             
         }
       };
   
     $.fn.marquee = function(m) 
     {
       var settings={
                     'delay':2000,
                     'duration':900,
                     'stop':true
                    };
       
       if(typeof m === 'object' || ! m)
       {
         if(m){ 
         $.extend( settings, m );
       }
 
         return methods.init.apply( this, [settings] );
       }
       else
       {
         return methods[m].apply( this);
       }
     };
   }
 )( jQuery );
 
 //Plugin end
 
 //call
 $(document).ready(
   function(){$('.slide').marquee({delay:3000});}
 );
 
 </script>
 </head>
 <body>

<!DOCTYPE html>
<html>
<head>
    <title>simple marquee with jquery</title>
</head>
<body>
<ul class="slide">
  <li><img src="http://www.google.com/logos/2010/canadianthanksgiving2010-hp.jpg"/></li>
  <li><img src="http://www.google.com/logos/2010/germany10-hp.gif"/></li>
  <li><img src="http://www.google.com/logos/stpatricks_02.gif"/></li>
</ul>
</body>
</html>

 </body>
 </html>