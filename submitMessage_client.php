<?php
require_once ('include/database.php');
require_once ('session.php');
?>
<?php
$adminUserId = 1; // Was told that admin's user Id would always be 1

$userId = $_SESSION['id'];

$q = "INSERT INTO `message` (`from`, `to`, `message`) VALUES (:from, :to, :message);";
$s = $dbh-> prepare ($q);
$s-> bindParam (':from', $userId);
$s-> bindParam (':to', $adminUserId);
$s-> bindParam (':message', $_POST['message']);
$s-> execute();

header('Location: messages_client.php');
?>