<!DOCTYPE html>
<?php

$id=$_GET['id'];
?>
<?php
/* Getting Latest Top Bar Notification */
$q = "SELECT * FROM `notification` WHERE `type`=1 ORDER BY `time` DESC LIMIT 1;";
$s = $dbh-> prepare ($q);
$s-> execute();
$r = $s-> fetch(PDO::FETCH_ASSOC);

$topbarNotification = $r;
/**/
?>

<html lang="en">
  <body>
<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
  <div class="panel panel-primary" >
    <div class="panel-body">
      <p align="center">
        <h3 align="center"><strong>Current Balance</strong> </h3>

        <h1 align="center">$100</h1> 

      <a href="add_funds.php">  <button type="button" class="btn btn-success btn-lg btn-block">Add Funds</button></a>
      
<h7 align="center"><strong> Add Funds for fast transaction !</strong></h7>
      </p>

    </div>
  </div>
  <div class="list-group">
    <a href="client_home.php?id=1" class="list-group-item <?php if($id==1) echo "active"; ?>"><span class="glyphicon glyphicon-dashboard"></span> My Dashboard</a>
    <a href="#cart"  id="cart_button" data-toggle="modal" class="list-group-item"> <span class="glyphicon glyphicon-shopping-cart"></span> My Shopping Cart</a>
    <a href="client_transactions.php?id=3" class="list-group-item <?php if($id==3) echo "active"; ?>"><span class="glyphicon glyphicon-briefcase"></span> My Transactions</a>
    <a href="#" class="list-group-item"><span class="glyphicon glyphicon-flag"></span> My Activities</a>
    <a href="#" class="list-group-item"><span class="glyphicon glyphicon-heart"></span> Refer a Friend</a>
    <a href="#" class="list-group-item"><span class="glyphicon glyphicon-phone-alt"></span> Need Help</a>
  </div>
   <div class="panel panel-primary" >
    <div class="panel-body">
      <p align="center">
        <strong>Feedback</strong>
        <br>
<small>        Please provide us your valuable feedback here</small>
        <form name="form1" action="feedback_submit.php">
         <textarea class="form-control" rows="3"></textarea>
         <br>
<button type="submit" class="btn btn-primary btn-sm btn-block" name="submmit" value="submit"> Submit feedback </button>
</form>
</text>
      
      
    </div>
  </div>
</div><!--/span-->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="offcanvas.js"></script>

    <script>
    $('.dropdown-toggle').dropdown();
    </script>
<script>
 
 //Plugin start
 (function($)
   {
     var methods = 
       {
         init : function( options ) 
         {
           return this.each(function()
             {
               var _this=$(this);
                   _this.data('marquee',options);
               var _li=$('>li',_this);
                   
                   _this.wrap('<div class="slide_container"></div>')
                        .height(_this.height())
                       .hover(function(){if($(this).data('marquee').stop){$(this).stop(true,false);}},
                              function(){if($(this).data('marquee').stop){$(this).marquee('slide');}})
                        .parent()
                        .css({position:'relative',overflow:'hidden','height':$('>li',_this).height()})
                        .find('>ul')
                        .css({width:screen.width*2,position:'absolute'});
           
                   for(var i=0;i<Math.ceil((screen.width*3)/_this.width());++i)
                   {
                     _this.append(_li.clone());
                   } 
             
               _this.marquee('slide');});
         },
      
         slide:function()
         {
           var $this=this;
           $this.animate({'left':$('>li',$this).width()*-1},
                         $this.data('marquee').duration,
                         'swing',
                         function()
                         {
                           $this.css('left',0).append($('>li:first',$this));
                           $this.delay($this.data('marquee').delay).marquee('slide');
             
                         }
                        );
                             
         }
       };
   
     $.fn.marquee = function(m) 
     {
       var settings={
                     'delay':2000,
                     'duration':900,
                     'stop':true
                    };
       
       if(typeof m === 'object' || ! m)
       {
         if(m){ 
         $.extend( settings, m );
       }
 
         return methods.init.apply( this, [settings] );
       }
       else
       {
         return methods[m].apply( this);
       }
     };
   }
 )( jQuery );
 
 //Plugin end
 
 //call
 $(document).ready(
   function(){$('.slide').marquee({delay:3000});}
 );
 
 
</script>
 </body>
</html>