<?php
session_start();
require_once ('include/database.php');
$register_name=$_POST['register_name'];
$register_email=$_POST['register_email'];
$register_password=$_POST['register_password'];
$register_re_password=$_POST['register_re_password'];
if($register_password===$register_re_password)
{
	$sql="select user_email from users where user_email=:register_email;";
	$stmt=$dbh->prepare($sql);
	$stmt->bindParam (':register_email', $register_email);
	$stmt->execute();
	if(!($stmt->fetch(PDO::FETCH_ASSOC)))
	{
		$register_password=md5($register_password);
		$sql="insert into users(user_name,user_password,user_email) values(:register_name,:register_password,:register_email);";
		$stmt=$dbh->prepare($sql);
		$stmt->bindParam (':register_name', $register_name);
		$stmt->bindParam (':register_password', $register_password);
		$stmt->bindParam (':register_email', $register_email);
		if($stmt->execute())
		{
			
			$sql="select * from users where user_email=:register_email;";
			$stmt=$dbh->prepare($sql);
			$stmt->bindParam (':register_email', $register_email);
			$stmt->execute();
			$r=$stmt->fetch(PDO::FETCH_ASSOC);
			$_SESSION['email']=$r['user_email'];
			$_SESSION['id']=$r['user_id'];
			$_SESSION['user_name']=$r['user_name'];
			echo "<script type="."text/javascript".">location.href = 'client_home.php?info=Thank You&serviceId=1 ';</script>";

		}
		else
			echo "<script type="."text/javascript".">location.href = 'index.php?message= Something Went Wrong Try Again!';</script>";
	}
	else
		echo "<script type="."text/javascript".">location.href = 'index.php?message= User Already Present';</script>";
}
else
	echo "<script type="."text/javascript".">location.href = 'index.php?message= Password Mismatch';</script>";
?>