<?php

require_once ('include/database.php');
if(isset($_POST['per']))
{
	$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$j=1;
	$coupon_value=$_POST['percentage'];
	$limit=$_POST['limit'];
	while($j<=$_POST['coupon_number'])
	{
		$res = "";
		for ($i = 0; $i < 10; $i++) {
			$res .= $chars[mt_rand(0, strlen($chars)-1)];
		}
		$q = "insert into coupons(coupon_id,coupon_value,min_amount,coupon_type) values(:res,:coupon_value,:limit,'percentage');";
		$s = $dbh-> prepare ($q);
		$s->bindParam (':res', $res);
		$s->bindParam (':coupon_value', $coupon_value);
		$s->bindParam (':limit', $limit);
		
		
		$s-> execute();
		
		$j++;
	}
}
else
if(isset($_POST['fix']))
{
	$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$j=1;
	$coupon_value=$_POST['fixed'];
	$limit=$_POST['limit1'];
	while($j<=$_POST['coupon_number1'])
	{
		$res = "";
		for ($i = 0; $i < 10; $i++) {
			$res .= $chars[mt_rand(0, strlen($chars)-1)];
		}
		$q = "insert into coupons(coupon_id,coupon_value,min_amount,coupon_type) values(:res,:coupon_value,:limit,'fixed')";
		$s = $dbh-> prepare ($q);
		$s->bindParam (':res', $res);
		$s->bindParam (':coupon_value', $coupon_value);
		$s->bindParam (':limit', $limit);
		
		$s-> execute();
		
		$j++;
	}
}
else
if(isset($_POST['static_fix']))
{
	$code=$_POST['static_coupon_code'];
	$amount=$_POST['fixed'];
	$min_amount=$_POST['limit1'];
	$date1=$_POST['date1'];
	$q = "insert into coupons(coupon_id,coupon_value,min_amount,coupon_type,expiry) values(:code,:amount,:min_amount,'static_fixed',:date1);";
	$s = $dbh-> prepare ($q);
	$s->bindParam (':code', $code);
	$s->bindParam (':amount', $amount);
	$s->bindParam (':min_amount', $min_amount);
	$s->bindParam (':date1', $date1);
	$s-> execute();		
		
		

}
else
if(isset($_POST['static_per']))
{
	$code=$_POST['static_coupon_code'];
	$per=$_POST['fixed'];
	$min_amount=$_POST['limit1'];
	$date1=$_POST['date1'];
	$q = "insert into coupons(coupon_id,coupon_value,min_amount,coupon_type,expiry) values(:code,:per,:min_amount,'static_percentage',:date1);";
	$s = $dbh-> prepare ($q);
	$s->bindParam (':code', $code);
	$s->bindParam (':per', $per);
	$s->bindParam (':min_amount', $min_amount);
	$s->bindParam (':date1', $date1);
	$s-> execute();		
		
		

}
else
echo "Error";
?>