<?php

	require_once ('include/database.php');
	//include "admin_head.php";
	//include "admin_sidepanel.php";

?>

<?php
	$q = "SELECT `id`, `name` FROM `serviceType`";
	$s = $dbh-> prepare ($q);
	$s-> execute();
	$r = $s-> fetchAll(PDO::FETCH_ASSOC);

	$serviceTypes = array();
	foreach ($r as $val)
		{
			$x=array('id'=>$val['id'], 'name'=>$val['name']);
		array_push ($serviceTypes,$x );
		}

	$q = "SELECT `name` FROM `service`";
	$s = $dbh-> prepare ($q);
	$s-> execute();
	$r = $s-> fetchAll(PDO::FETCH_ASSOC);

	$services = array();
	foreach ($r as $val)
		array_push ($services, $val['name']);
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Artist Promotion</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">

		<link href="css/offcanvas.css" rel="stylesheet">
	</head>

	<body>

<?php

	include "admin_head.php";
	include "admin_sidepanel.php";

?>
		

		
			
				
				<div class="col-xs-12 col-sm-9">
					
					<div class="row">

							<form action="service.php" method='POST' role="form">
							<select name='serviceType' class="form-control">
<?php foreach ($serviceTypes as $val) { ?>
								<option value="<?php echo $val['id'] ?>"><?php echo $val['name'] ?></option>
<?php } ?>
							</select>
							<br>
							<input type="submit" class="btn btn-default" value="Submit">
							</form>


					</div><!--/row-->


				</div><!--/row-->

			<hr>

			<footer>
				<p>&copy; Company 2013</p>
			</footer>

		

		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="offcanvas.js"></script>

		<script>
		$('.dropdown-toggle').dropdown();
		</script>

	</body>
</html>