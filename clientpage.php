<?php
require_once 'session.php';
require_once 'session.php';
?>

<?php
require_once ('include/database.php');
?>
<?php
if (isset($_GET['productId']) && !isset($_GET['serviceId']))
{
	$q = "SELECT `serviceId` FROM `product` WHERE `id`=:productId;";
	$s = $dbh-> prepare ($q);
	$s-> bindParam (':productId', $_GET['productId']);
	$s-> execute();
	$r = $s-> fetch(PDO::FETCH_ASSOC);

	$serviceId = $r['serviceId'];

	header ("Location: clientpage.php?serviceId=$serviceId&productId=".$_GET['productId']);
}

$activeServiceId = $_GET['serviceId'];

$q = "SELECT `id`, `name`, `iconFilename` FROM `service`;";
$s = $dbh-> prepare ($q);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$allServices = $r;

$inactiveServices = array();
foreach ($r as $val)
{
  if ($val['id'] == $activeServiceId)
    $activeService = $val;
  else
    array_push ($inactiveServices, $val);
}


$q = "SELECT `id`, `name` FROM `product` WHERE `serviceId`=:serviceId;";
$s = $dbh-> prepare ($q);
$s-> bindParam (':serviceId', $activeServiceId);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$products = $r;
$activeProduct = $products[0]['id'];

$questionMarks = str_repeat('?, ', count($r)-1) . '?';
$q = "SELECT * FROM `item` WHERE `productType` IN ($questionMarks);";
$s = $dbh-> prepare ($q);
foreach ($products as $k=>$val)
  $s-> bindParam ($k+1, $val['id']);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$items = $r;
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <style>
    #image
    {
      opacity:0.4;

    }
	
	body
{
background-image:url('ae.jpg');
background-attachment:fixed; 


}

  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Client Home</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/offcanvas.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      <script>
        var activeProduct = <?php echo $activeProduct; ?>;
        var items = <?php echo json_encode ($items) ?>;
        var products = <?php echo json_encode($products) ?>;
        var selectedItems = [];
        var currOptionalItems = [];
        var currOptionalItemCost;
        var currOptionalItemId;
      </script>

    </head>

    <body>
      <?php

      include "client_head.php";

      ?>
      <div class="container">

        <?php

        include "client_sidepanel.php";

        ?>


        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

          <div class="header">
            <div class="navbar navbar-right">

              <div class="btn-group">
                <button type="button" class="btn btn-primary btn-primary dropdown-toggle" style=" background-color:#FF3333;" data-toggle="dropdown">
                  <?php echo $activeService['name'] ?> <span class="caret"></span>
                </button>
                <ul id="services" class="dropdown-menu" role="menu">
                  <?php foreach ($inactiveServices as $val) { ?>
                  <li id="service_<?php echo $val['id'] ?>"><a href="#"><?php echo $val['name'] ?></a></li>
                  <?php } ?>
                </ul>
              </div>
            </div >          
          </div>

          <div class="container">

            <div class="row">
              <div class="col-md-7">



<div class="progress progress-striped active">
  <div class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100"    style="width: 33%;  background-color: green;">
    <span class="sr-only">33% Complete</span>
33% Famous  </div>
</div>



                <ul id="products" class="nav nav-tabs" style="background:;">
                  <?php foreach ($products as $k=>$product) { ?>
                  <li <?php echo "id=\"product_".$product['id']."\"" ?><?php if ($k == 0) echo " class=\" active\"" ?>><a href="#"><?php echo "<b style='color:rgb(76,0,153);'>".$product['name']."</b>" ?></a></li>

                  <?php } ?>
                </ul>

 
<br>
<form id="payment" action="temp_checkout.php" method="post">

<div class="input-group input-group-lg">
 
  <span class="input-group-addon">@</span>
  <input type="text" class="form-control" name="url"  placeholder="URL of the Video">
</div>

</div>
  <div class="col-sm-7">
<br>
  <br>
  
			  <br>
              <div class="row">
                <div id="items" style="cursor:pointer;">
                </div>
              </div>
  </div>
  <div  class="col-sm-2" >
<br><br><br><br><br><br><br>
  <div class="row" style=" margin:0px ; padding:0px;">
        <div id="optionalItems" style="border:2px solid black;">      
    <img src="img/serviceIcon/<?php echo $activeService['iconFilename'] ?>" class="pull-left" style="height: 32px; width: 32px;">

    <p align="center" id="ItemName_optional"></p>

    <p align="center" id="ProductName_optional"></p>

    <div class="text-center">
      <button onClick="setNextOptionalItem()" type="button" class="leChild"><span class="glyphicon glyphicon-arrow-up"></span></button>
      <h2 align="center">$<span id="cost_optional" ></span></h2>
      <button onClick="setPrevOptionalItem()" type="button" class="leChild"><span class="glyphicon glyphicon-arrow-down"></span></button>
    </div>
    <p align="center" id="desc_optional"></p>
  </div>
  </div>
    </div>    
        </div><!--/row-->
          </div><!--/span-->

        </div><!--/row-->

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

          <div class="header">
            <div class="navbar ">
            <hr>

              
			  <h3 style=" text-align:center;">Balance = <div id="totalCost" style="display: inline;">0</div> $</h3>


            <button type="submit" name="paypal" class="btn btn-primary btn-lg" style="margin-left:auto;margin-right:auto;display:block;margin-top:5%;margin-bottom:0%; background-color: red;" >Proceed</button>  
</form>
            </div >          
          </div>



        </div><!--/container-->
        
<div class="modal fade" style="background:rgba(102,0,204,0.1);" id="cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"style="background:rgba(0,51,102,1);">
        <button type="button" id="cart_close" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel" style="color:white;">My Shopping Cart</h4>
      </div>
      <div class="modal-body" style="background:rgba(0,51,102,.0);">
      

       <!--   Login form   -->
       <table class="table table-hover" id="cart_table">
        
       </table>
      
     

        

     
    <div class="modal-footer">
   <small><ul class="list-group text-left "> <li>This cart shows you all the products that you have selected.</li>
    <li>You may choose to remove any products from the cart if you want by clicking X.</li>

<li>    Click Proceed to go to the payment page.</li>
   </small></ul>
        </div>

    
  </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
 
</div><!-- /.modal -->




        <hr>

        <footer>
          <p>&copy; Company 2013</p>
        </footer>

      </div><!--/.container-->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
     <script>
      $('.dropdown-toggle').dropdown();
    </script>

    <script>
      $('#products li').click( function (e) 
      {
        var id = $(this).attr('id');
        id = id.substr(id.indexOf("_") + 1);

        $('li[id^="product_"]').removeClass ('active');
        $('#product_'+id).addClass ('active');

        productId = id;

        $('#items').empty();

        var ct = 0;
        appendString = "";
        for (var i in items)
        {
          if (items[i]['productType'] == productId && items[i]['optional'] == 0)
          {
            if (ct%3==0)
              appendString += '<div class="row" style="margin-top: 10px; margin-left: 0px; margin-right: 0px">';

            appendString += '<div id="item_'+items[i]['id']+'" class="col-sm-3';
            if (ct%3!=0) appendString += ' col-sm-offset-1';
            appendString += '" style="border:2px solid black;';
            if ($.inArray(items[i]['id'], selectedItems) != -1) appendString += ' background-color: rgb(102, 0, 102); color: white;';
            appendString += '">';
            appendString += '<img src="img/serviceIcon/'+"<?php echo $activeService['iconFilename'] ?>"+'" class="pull-left" style="height: 32px; width: 32px; margin-left: -15px">';
            appendString += '<p align="center">'+items[i]['name']+'</p>';
            var productName;
            for(var j in products) { if (products[j]['id']==productId) productName=products[j]['name']}
              appendString += '<p align="center">'+productName+'</p>';
            appendString += '<h2 align="center" id="cost_item_'+items[i]['id']+'">$'+items[i]['cost']+'</h2>';
            appendString += '<p align="center">'+items[i]['desc']+'</p>';
            appendString += '</div>';

            ct ++;

            if ( (ct==items.length) || (ct%3==0) )
            {
              appendString += '</div>';
            }
          }
        }

        $('#items').append(appendString);
        $('#items').hide();
        $('#items').fadeIn();
        $('.aPopover').popover();
        activeProduct = productId;

        $('div[id^="item_"]').click (onItemClick);

        $('#ProductName_optional').html (productName);
        currOptionalItems.length = 0;

        for (var i in items)
        {
          if (items[i]['productType']==activeProduct && items[i]['optional']=='1')
          {
            currOptionalItems.push(items[i]);
          }
        }
        currOptionalItems.sort (function (a, b)
          {
            return (a['cost'] - b['cost'])
          });

        currOptionalItemCost = -1;
        currOptionalItemId = -1;
        setNextOptionalItem ();
      });
</script>

<script>
function setNextOptionalItem ()
{
  index = $.inArray(currOptionalItemId, selectedItems);
  if (index != -1) // Not in array
  {
    selectedItems.splice(index, 1);
    $('#optionalItems').css({'background-color':'transparent', 'color':'black'});
    $('#totalCost').html( parseFloat($('#totalCost').html()) - parseFloat(currOptionalItemCost) );
  }

  var ct = 0;
  for (var i in currOptionalItems)
  {
    if ( (parseFloat(currOptionalItems[i]['cost']) >= parseFloat(currOptionalItemCost)) && (currOptionalItems[i]['id'] != currOptionalItemId) )
    {
      setCurrentOptionalItem(currOptionalItems[i]);
      break;
    }
  }

  if (ct==0 && currOptionalItemCost==-1)
    $('#optionalItems').fadeOut();
  else
    $('#optionalItems').fadeIn("slow");
}
function setPrevOptionalItem ()
{
  index = $.inArray(currOptionalItemId, selectedItems);
  if (index != -1) // Not in array
  {
    selectedItems.splice(index, 1);
    $('#optionalItems').css({'background-color':'transparent', 'color':'black'});
    $('#totalCost').html( parseFloat($('#totalCost').html()) - parseFloat(currOptionalItemCost) );
  }

  for (var i = currOptionalItems.length-1; i >= 0; i-- )
  {
    if ( (parseFloat(currOptionalItems[i]['cost']) <= parseFloat(currOptionalItemCost)) && (currOptionalItems[i]['id'] != currOptionalItemId) )
    {
      setCurrentOptionalItem(currOptionalItems[i]);
      break;
    }
  }
}

function setCurrentOptionalItem (item)
{
  currOptionalItemCost = item['cost'];
  currOptionalItemId = item['id'];

  $('#ItemName_optional').html (item['name']);
  $('#cost_optional').html (item['cost']);
  $('#desc_optional').html (item['desc']);
}
</script>

<script>
$('#optionalItems').click (function ()
{
  var id = currOptionalItemId;

  var cost = $('#cost_optional').html();

  index = $.inArray(id, selectedItems);
  if (index == -1) // Not in array
  {
    selectedItems.push(id);

    $('#totalCost').html( parseFloat($('#totalCost').html()) + parseFloat(cost) );

    $(this).css({'background-color' :'rgb(102,0,102)','color':'white'});

  }
  else
  {
    selectedItems.splice(index, 1);

    $('#totalCost').html( parseFloat($('#totalCost').html()) - parseFloat(cost) );

    $(this).css({'background-color':'red', 'color':'black'});
  }
}).children().children('.leChild').click(function(e) {
  return false;
});
</script>

<script>
  $('#services li').click( function (e) 
  {
    var id = $(this).attr('id');
    id = id.substr(id.indexOf("_") + 1);

    var oldUrl = window.location.href;

    var newUrl = oldUrl.substr(0, oldUrl.indexOf("=")+1) + id;

    window.location.href = newUrl;
  });
</script>

<script>
$('div[id^="item_"]').click (onItemClick);

function onItemClick()
{
	var id = $(this).attr('id');
  id = id.substr(id.indexOf("_") + 1);

  var cost = $('#cost_item_'+id).html();
  cost = cost.substr(1, cost.length);

  index = $.inArray(id, selectedItems);
  if (index == -1) // Not in array
  {
  	selectedItems.push(id);

  	$('#totalCost').html( parseFloat($('#totalCost').html()) + parseFloat(cost) );

  	$(this).css({'background-color' :'rgb(102,0,102)','color':'white','border': '3px solid rgb(255,255,255)'});

  }
  else
  {
  	selectedItems.splice(index, 1);

  	$('#totalCost').html( parseFloat($('#totalCost').html()) - parseFloat(cost) );

  	$(this).css({'background-color':'transparent', 'color':'black'});
  }
}

$('#payment').submit(function() {
  for (var i in selectedItems)
  {
  	$('#payment').append('<input type="hidden" name="item[]" value="Item #'+selectedItems[i]+'">');
  }
});
$('#cart_button').click(function() {

$('#cart_table').html('');
  
  $('#cart_table').append('<tr><th>S No. </th><th>Item Id</th><th>Product</th><th>Cost</th><th> Action</th></tr>');
var count=1;
    for (var i in selectedItems)
  {
    var cost = $('#cost_item_'+selectedItems[i]).html();
    cost = cost.substr(1, cost.length);
var prod = $('#prod_name_'+selectedItems[i]).html();
    //prod = prod.substr(0, prod.length);


    $('#cart_table').append('<tr id="x_'+selectedItems[i]+'"><td> '+count+' </td><td>Item '+selectedItems[i]+' <td>'+prod+'</td><td>$'+cost+' </td><td><div id="items_'+selectedItems[i]+'"><button   onClick="cart1(this.id)" id="button_'+selectedItems[i]+'"type="button" ><span class="glyphicon glyphicon-remove"></span></button> </div></tr><br>');

  
count ++;
  }

});

$('#cart_close').click(function(){
$('#cart_table').html("");
});
</script>
 <script >
 function cart1(clicked_id)
 { //$(this).html("fdfd");
  id = clicked_id;
  id = id.substr(id.indexOf("_") + 1);
  //alert(id);
  $('#x_'+id).remove();
   index = $.inArray(id, selectedItems);
  // // alert(clicked_id);
  selectedItems.splice(index, 1);
  var cost = $('#cost_item_'+id).html();
  cost = cost.substr(1, cost.length);
  $('#totalCost').html( parseFloat($('#totalCost').html()) - parseFloat(cost) );

  $("#item_"+id).css({'background-color':'transparent', 'color':'black'});
}
</script>
 <script>
 $('.aPopover').popover();
 </script>

<script>
$('#product_'+products[0]['id']).trigger('click');
</script>

<script src="https://raw.github.com/Fa773NM0nK/Fa773N_M0nK-library/master/JavaScript/Get%20URL%20Parameter/getURLParameter.js"></script>
<script>
if (getURLParameter('productId') != "")
{
  $("#product_"+getURLParameter('productId')).trigger("click");
}
</script>