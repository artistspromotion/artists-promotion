<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Admin</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">

		<link href="css/offcanvas.css" rel="stylesheet">
	</head>

	<body>


		<div class="container">

			<?php 
$id=$_GET['id'];
			?>
				<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">

	<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
Products        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse ">
      <div class="panel-body">
	     <a href="admin.php?id=1" class="list-group-item <?php if($id==1) echo "active";  ?>">Generate Products</a>
		<a href="update_products.php?id=5" class="list-group-item <?php if($id==5) echo "active";  ?>">Update/Delete Products</a>
      </div>
    </div>
  </div>

<!-- 2 -->
<div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapsetwo">
Notifications        </a>
      </h4>
    </div>
    <div id="collapsetwo" class="panel-collapse collapse ">
      <div class="panel-body">
	     <a href="notification_make.php?id=2" class="list-group-item <?php if($id==2) echo "active";  ?>">Generate Notifications</a>
						<a href="manage_notifications.php?id=11" class="list-group-item <?php if($id==11) echo "active";  ?>">Manage Notifications</a>
						  </div>
    </div>
  </div>

<!-- 3 -->
<div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree">
Messages       </a>
      </h4>
    </div>
    <div id="collapsethree" class="panel-collapse collapse ">
      <div class="panel-body">
	    <a href="admin_messages.php?id=3" class="list-group-item <?php if($id==3) echo "active";  ?>">Messages</a>
										  </div>
    </div>
  </div>

  <!-- 4 -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
Offers      </a>
      </h4>
    </div>
    <div id="collapsefour" class="panel-collapse collapse ">
      <div class="panel-body">
	  <a href="add_offer.php?id=4" class="list-group-item <?php if($id==4) echo "active";  ?>">Offers</a>
															  </div>
    </div>
  </div>

  <!-- 5 -->


  <!-- 6 -->
   <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapsesix">
Orders      </a>
      </h4>
    </div>
    <div id="collapsesix" class="panel-collapse collapse ">
      <div class="panel-body">
	 
<a href="manage_order.php?id=8" class="list-group-item <?php if($id==8) echo "active";  ?>">Manage Orders</a>
	<a href="get_reports.php?id=7" class="list-group-item <?php if($id==7) echo "active";  ?>">Get Reports</a>
											

	 </div>
    </div>


  </div>
<!-- 7 -->
   <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseseven">
Coupons      </a>
      </h4>
    </div>
    <div id="collapseseven" class="panel-collapse collapse ">
      <div class="panel-body">
	 
<a href="generate_coupons.php?id=9" class="list-group-item <?php if($id==9) echo "active";  ?>">Create Coupons</a>
						<a href="view_coupon.php?id=10" class="list-group-item <?php if($id==10) echo "active";  ?>">Manage Coupons</a>
																	

	 </div>
    </div>


  </div>
  </div>



					<div class="list-group">
						
						<a href="#" class="list-group-item">Need Help</a>
						
						<a href="create_user.php?id=6" class="list-group-item <?php if($id==6) echo "active";  ?>">Create User</a>
						</div>

				
				
		</div><!--/.container-->


		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="offcanvas.js"></script>

		<script>
		$('.dropdown-toggle').dropdown();
		</script>

	</body>
</html>