  <?php
  $serviceType = $_POST['serviceType'];
  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>Artist Promotion</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/offcanvas.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
      </head>

      <body>
        <?php 
        include "admin_head.php";
        include "admin_sidepanel.php";

        ?>
<div class="col-xs-12 col-sm-9">
        <div class="row">
          <form role="form" method="POST" action="product.php" enctype="multipart/form-data">
            <input type="hidden" name="type" value="<?php echo $serviceType ?>">
            <div class="form-group">
              <input type="text" class="form-control"  name="name" placeholder="Service name example: You Tube ,Vimeo .">
            </div>
            <div class="form-group">
              <label for="exampleInputFile">Image</label>
              <input type="file" id="exampleInputFile" name="image">
              <p class="help-block">Upload an image that you would like your users to see.</p>
            </div>
            <div class="form-group">
              <label for="icon">Icon</label>
              <input type="file" id="icon" name="icon">
              <p class="help-block">Upload an icon that you would like your users to see.</p>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div><!--/span-->
</div>
        <hr>

      <footer>
        <p>&copy; Company 2013</p>
      </footer>




      <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="offcanvas.js"></script>
      <script>
        $('.dropdown-toggle').dropdown();
      </script>

    </body>
    </html>

