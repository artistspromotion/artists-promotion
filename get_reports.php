<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Artist Promotion</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">

		<link href="css/offcanvas.css" rel="stylesheet">
	</head>

	<body>
	<?php

require_once ('include/database.php');

	include "admin_head.php";
	include "admin_sidepanel.php";

?>
		

	<div class="container">

		<form name="form1" method="POST" action="get_reports.php">

<select name="category">
<option value='1'>Sales by day</option>
<option value='2'>Sales by week</option>
<option value='3'>Sales by month</option>
<option value='4'>Sales by year</option>

</select>
<input type="submit" name="submit" value="submit">

</form>
<div class="col-md-6">
<table class="table">

<?php
//require_once ('include/database.php');
$x=$_POST['category'];

echo $x;


echo "<br>";

if($x==1)
{
$q1 = "SELECT `payment_date`,sum(`item_gross`) FROM transactions group by `payment_date`";
$s1 = $dbh-> prepare ($q1);
$s1-> execute();
$r1 = $s1-> fetchAll(PDO::FETCH_ASSOC);
 foreach($r1 as $val)
 {echo "<tr>";
echo "<td>".$val['date(`payment_date`)'];
echo "<td>".$val['sum(`item_gross`)'];
 echo "<br>";
 }

}

if($x==2)
{
$q1 = "SELECT week(`payment_date`),sum(`item_gross`) FROM transactions group by week(`payment_date`), month(`payment_date`)";
$s1 = $dbh-> prepare ($q1);
$s1-> execute();
$r1 = $s1-> fetchAll(PDO::FETCH_ASSOC);
 foreach($r1 as $val)
 {echo "<tr>";
echo "<td>".$val['week(`payment_date`)'];
echo "<td>".$val['sum(`item_gross`)'];
 echo "<br>";
 }

}




if($x==3)
{
$q1 = "SELECT month(`payment_date`),sum(`item_gross`) FROM transactions group by month(`payment_date`), year(`payment_date`)";
$s1 = $dbh-> prepare ($q1);
$s1-> execute();
$r1 = $s1-> fetchAll(PDO::FETCH_ASSOC);
 foreach($r1 as $val)
 {echo "<tr>";
echo "<td>".$val['month(`payment_date`)'];
echo "<td>".$val['sum(`item_gross`)'];
 echo "<br>";
 }

}
if($x==4)
{
$q1 = "SELECT year(`payment_date`),sum(`item_gross`) FROM transactions group by year(`payment_date`)";
$s1 = $dbh-> prepare ($q1);
$s1-> execute();
$r1 = $s1-> fetchAll(PDO::FETCH_ASSOC);
 foreach($r1 as $val)
 {echo "<tr>";
echo "<td>".$val['year(`payment_date`)'];
echo "<td>".$val['sum(`item_gross`)'];
 echo "<br>";
 }

}

?>
</table>
</div>

	</div>

 <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>

	</body>
</html>