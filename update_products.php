

<?php
require_once ('include/database.php');
?>
<?php
$q = "SELECT `id`, `name` FROM `service`;";
$s = $dbh-> prepare ($q);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$allServices = $r;

if (isset ($_GET['serviceId']))
  $activeServiceId = $_GET['serviceId'];
else
  $activeServiceId = $allServices[0]['id'];

$inactiveServices = array();
foreach ($r as $val)
{
  if ($val['id'] == $activeServiceId)
    $activeService = $val['name'];
  else
    array_push ($inactiveServices, $val);
}


$q = "SELECT `id`, `name` FROM `product` WHERE `serviceId`=:serviceId;";
$s = $dbh-> prepare ($q);
$s-> bindParam (':serviceId', $activeServiceId);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$products = $r;
$activeProduct = $products[0]['id'];

$questionMarks = str_repeat('?, ', count($r)-1) . '?';
$q = "SELECT * FROM `item` WHERE `productType` IN ($questionMarks);";
$s = $dbh-> prepare ($q);
foreach ($products as $k=>$val)
  $s-> bindParam ($k+1, $val['id']);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$items = $r;
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <style>
    #image
    {
      opacity:0.4;

    }

  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Client Home</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/offcanvas.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      <script>
        var activeProduct = <?php echo $activeProduct; ?>;
        var items = <?php echo json_encode ($items) ?>;
        var products = <?php echo json_encode($products) ?>;
      </script>

    </head>

    <body>
      <?php

      include "admin_head.php";

      ?>
      <div class="container">

        <?php

        include "admin_sidepanel.php";

        ?>


        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

          <div class="header">
            <div class="navbar navbar-right">

              <div class="btn-group">
                <button type="button" class="btn btn-primary btn-primary dropdown-toggle" data-toggle="dropdown">
                  <?php echo $activeService ?> <span class="caret"></span>
                </button>
                <ul id="services" class="dropdown-menu" role="menu">
                  <?php foreach ($inactiveServices as $val) { ?>
                  <li id="service_<?php echo $val['id'] ?>"><a href="#"><?php echo $val['name'] ?></a></li>
                  <?php } ?>
                </ul>
              </div>
            </div >          
          </div>

          <div class="container">

            <div class="row">
              <div class="col-md-7">



                


                  <ul id="products" class="nav nav-tabs" style="background:;">
                    <?php foreach ($products as $k=>$product) { ?>
                    <li <?php echo "id=\"product_".$product['id']."\"" ?><?php if ($k == 0) echo " class=\" active\"" ?>><a href="#"><?php echo "<b style='color:rgb(76,0,153);'>".$product['name']."</b>" ?>  

 <p align="center"><button class="btn btn-primary btn-sm" type="button" name="edit1" value="Edit or delete" id="modal_call2" onclick="get_val1('<?php echo $product['id'] ?>','<?php echo $product['name'] ?>')">
Edit or delete


</button>


                    </a> </li> 

                    <?php } ?>
                    <li>
                      <form action="product.php" method="POST">
                        <input type="hidden" name="serviceId" value="<?php echo $activeServiceId ?>">
                        <button type="submit" class="btn btn-xs btn-success"> <span class="glyphicon glyphicon-plus"></span> </button>
                      </form>
                    </li>
                  </ul>


                  <br>

                    


                    <br>
                    <div id="items">
                      <?php
                      $ct = 0;

                      foreach ($items as $item)
                      {
                        if ($item['productType'] == $activeProduct)
                        {
                          if ($ct%3==0)
                          {
                            ?><div class="row" style="margin-top: 10px; margin-left: 0px; margin-right: 10px"><?php
                          }
                          ?>
                          <div id="item_<?php echo $item['id'] ?>" class="col-sm-4 col-lg-3 <?php if ($ct%3!=0) echo "col-lg-offset-1" ?>" style="border:2px solid black; ">


                            <p align="center"><?php echo $item['name'] ?></p>

                            <p align="center"><?php echo $products[0]['name'] ?></p>

                            <h2 id="cost_item_<?php echo $item['id'] ?>" align="center">$<?php echo $item['cost'] ?></h2>



                            <p align="center"><?php echo $item['desc'] ?></p>
                            <p align="center"><button class="btn btn-primary btn-sm" type="button" name="edit" value="Edit or delete" id="modal_call" onclick="get_val('<?php echo $item['name'] ?>','<?php echo $item['id'] ?>','<?php echo $item['cost'] ?>','<?php echo $item['desc'] ?>')">
Edit or delete


</button>
</p>

                          </div>
                          <?php

                          $ct ++;

                          if ( ($ct==count($items)) || ($ct%3==0) )
                          {
                            ?></div><?php
                          }
                        }
                      }
                      ?>

                      <form id="updateItemForm" action="item.php?productId=" method="POST">
                        <input type="hidden" name="serviceId" value="<?php echo $activeServiceId ?>">
                        <a href="#" id="addMoreItemsSubmit">ADD more Items </a>
                      </form>

                      <script>
                      $('#addMoreItemsSubmit').click (function ()
                      {
                        $('#updateItemForm').attr('action', $('#updateItemForm').attr('action')+activeProduct);
                        $('#updateItemForm').submit();
                      });
                      </script>

                    </div>

                  </div>
                </div><!--/row-->
              </div><!--/span-->

            </div><!--/row-->

            <div class="col-xs-12 col-sm-9">
              <p class="pull-right visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
              </p>

            </div>

            <form action="update_product_backend.php" method="POST">
            	<input type="hidden" name="serviceIdToDelete" value="<?php echo $activeServiceId ?>">
            	<input type="hidden" name="serviceName" value="<?php echo $activeService ?>">
            	<button type="submit" class="btn btn-warning pull-right">Delete Sevice</button>
          	</form>

            <hr>

          </div><!--/container-->
<!-- update modal -->
          <div class="modal fade" style="background:rgba(102,0,204,0.1);" id="update_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header"style="background:rgba(0,51,102,1);">
                  <button type="button" id="cart_close" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel" style="color:white;">Update /Delete</h4>
                </div>
                <div class="modal-body" style="background:rgba(0,51,102,.0);">
                  <!--   Login form   -->
                 <form  role="form" class="form-horizontal" name="form1" method="POST" action="update_product_backend.php"  id="change_form">


                  </form>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
<!-- update modal end -->

<script>
function setModalSubmitHandlers ()
{
	$('#itemUpdate').click ( function () 
		{
			$('input.form-control').each ( function () 
				{
					if ($(this).val() == "")
						$(this).val($(this).attr('placeholder'));
				});

			return true;
		});

	$('#itemDelete').click ( function () 
		{
			$('#change_form').append('<input type="hidden" name="delete" value="true">');

			return true;
		});
}
</script>

<script language="javascript">
function  get_val(a,b,c,d)
{
	$('#change_form').html('<input type="hidden" name="itemId" value="'+b+'"><div class="form-group"><label for="inputPassword" class="col-sm-2 control-label">Name</label>    <div class="col-sm-10"><input type="text" name="name" class="form-control" placeholder="'+a+'">    </div>  </div>  <div class="form-group"><label for="inputPassword" class="col-sm-2 control-label">Cost</label>    <div class="col-sm-10"><input type="text" name="cost" class="form-control" placeholder="'+c+'">    </div>  </div><div class="form-group"><label for="inputPassword" class="col-sm-2 control-label">Description</label>    <div class="col-sm-10"><input type="text" name="desc" class="form-control" placeholder="'+d+'"></div></div><hr><div class="form-group"><div class="col-sm-2 pull-right"><button type="submit" class="btn btn-danger form-control" id="itemDelete">Delete</button></div><div class="col-sm-2 pull-right"><button type="submit" class="btn btn-success form-control" id="itemUpdate">Update</button></div></div>');
	$('#update_form').modal('show');
	setModalSubmitHandlers();
}

function  get_val1(a,b)
{
	$('#change_form').html('<input type="hidden" name="productId" value="'+a+'"><div class="form-group"><label for="inputPassword" class="col-sm-2 control-label">Name</label>    <div class="col-sm-10"><input type="text" name="name" class="form-control" placeholder="'+b+'">    </div>  </div>  <hr><div class="form-group"><div class="col-sm-2 pull-right"><button type="submit" class="btn btn-danger form-control" id="itemDelete">Delete</button></div><div class="col-sm-2 pull-right"><button type="submit" class="btn btn-success form-control" id="itemUpdate">Update</button></div></div>');
	$('#update_form').modal('show');
	setModalSubmitHandlers();
}


</script>
<div class="modal fade" style="background:rgba(102,0,204,0.1);" id="cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"style="background:rgba(0,51,102,1);">
        <button type="button" id="cart_close" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel" style="color:white;">Shopping Cart</h4>
      </div>
      <div class="modal-body" style="background:rgba(0,51,102,.0);">


       <!--   Login form   -->
       <table id="cart_table">

       </table>






       <div class="modal-footer">

       </div>


     </div>
   </div><!-- /.modal-content -->
 </div><!-- /.modal-dialog -->
 
</div><!-- /.modal -->




<hr>

<footer>
  <p>&copy; Company 2013</p>
</footer>

</div><!--/.container-->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      $('.dropdown-toggle').dropdown();
    </script>

    <script>
      $('#products li').click( function (e) 
      {
        var id = $(this).attr('id');
        id = id.substr(id.indexOf("_") + 1);

        $('li[id^="product_"]').removeClass ('active');
        $('#product_'+id).addClass ('active');

        productId = id;

        $('#items').empty();

        var ct = 0;
        appendString = "";
        for (var i in items)
        {
          if (items[i]['productType'] == productId)
          {
            if (ct%3==0)
              appendString += '<div class="row" style="margin-top: 10px; margin-left: 0px; margin-right: 0px">';

            appendString += '<div id="item_'+items[i]['id']+'" class="col-sm-4 col-lg-3';
            if (ct%3!=0) appendString += ' col-lg-offset-1';
            appendString += '" style="border:2px solid black; ">';
            appendString += '<p align="center">'+items[i]['name']+'</p>';
            var productName;
            for(var j in products) { if (products[j]['id']==productId) productName=products[j]['name']}
              appendString += '<p align="center">'+productName+'</p>';
            appendString += '<h2 align="center" id="cost_item_'+items[i]['id']+'">$'+items[i]['cost']+'</h2>';
            appendString += '<p align="center">'+items[i]['desc']+'</p>';
            appendString += '</div>';

            ct ++;

            if ( (ct==items.length) || (ct%3==0) )
            {
              appendString += '</div>';
            }
          }
        }

        $('#items').append(appendString);
        $('#items').hide();
        $('#items').fadeIn();
        $('.aPopover').popover();
        activeProduct = productId;
      });
</script>

<script>
  $('#services li').click( function (e) 
  {
    var id = $(this).attr('id');
    id = id.substr(id.indexOf("_") + 1);

    var newUrl = "update_products.php?serviceId=" + id;

    window.location.href = newUrl;
  });
</script>

<script>
  var selectedItems = [];

  $('div[id^="item_"]').click( function() {
	//alert("hi");
	var id = $(this).attr('id');
  id = id.substr(id.indexOf("_") + 1);

  var cost = $('#cost_item_'+id).html();
  cost = cost.substr(1, cost.length);

  index = $.inArray(id, selectedItems);
  if (index == -1) // Not in array
  {
  	selectedItems.push(id);

  	$('#totalCost').html( parseFloat($('#totalCost').html()) + parseFloat(cost) );

  	$(this).css({'background-color' :'rgb(102,0,102)','color':'white'});

//    $(this).css('color', 'white',);

}
else
{
 selectedItems.splice(index, 1);

 $('#totalCost').html( parseFloat($('#totalCost').html()) - parseFloat(cost) );

 $(this).css({'background-color':'transparent', 'color':'black'});
}
});

  $('#payment').submit(function() {
    for (var i in selectedItems)
    {
     $('#payment').append('<input type="hidden" name="item[]" value="Item #'+selectedItems[i]+'">');
   }
 });
  $('#cart_button').click(function() {

    for (var i in selectedItems)
    {
      var cost = $('#cost_item_'+selectedItems[i]).html();
      cost = cost.substr(1, cost.length);
      $('#cart_table').append('<tr id="x_'+selectedItems[i]+'">  <td>Item '+selectedItems[i]+' <td> <td>Cost'+cost+' </td><td><div id="items_'+selectedItems[i]+'"><button   onClick="cart1(this.id)" id="button_'+selectedItems[i]+'"type="button" >but</button> </div></tr><br>');

    }

  });

  $('#cart_close').click(function(){
    $('#cart_table').html("");
  });
</script>
<script >
 function cart1(clicked_id)
 { //$(this).html("fdfd");
 id = clicked_id;
 id = id.substr(id.indexOf("_") + 1);
 alert(id);
 $('#x_'+id).remove();
 index = $.inArray(id, selectedItems);
  // // alert(clicked_id);
  selectedItems.splice(index, 1);
  var cost = $('#cost_item_'+id).html();
  cost = cost.substr(1, cost.length);
  $('#totalCost').html( parseFloat($('#totalCost').html()) - parseFloat(cost) );

  $("#item_"+id).css({'background-color':'transparent', 'color':'black'});
}
</script>
<script>
 $('.aPopover').popover();
</script>

