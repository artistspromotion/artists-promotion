<?php
require_once ('include/database.php');
?>
<?php
if (isset ($_POST['notificationDeleteId']))
{
	$q = "DELETE FROM `notification` WHERE `id`=:id;";
	$s = $dbh-> prepare($q);
	$s-> bindParam (':id', $_POST['notificationDeleteId']);
	$s-> execute();

	array_map('unlink', glob('img/notificationImage/notification_'.$_POST['notificationDeleteId'].'.*'));
}
else
{
	$q = "UPDATE `notification` SET `title`=:title, `text`=:text WHERE `id`=:id;";
	$s = $dbh-> prepare($q);
	$s-> bindParam (':title', $_POST['title']);
	$s-> bindParam (':text', $_POST['text']);
	$s-> bindParam (':id', $_POST['id']);
	$s-> execute();
}

header('Location: ' . $_SERVER['HTTP_REFERER']);
?>