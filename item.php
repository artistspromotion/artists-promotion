<?php
require_once ('include/database.php');
?>
<?php
$serviceId = $_POST['serviceId'];
if (isset ($_POST['name']) )
{  
  $names = $_POST['name'];
  $descs = $_POST['desc'];

  $q = "INSERT INTO `product` (`serviceId`, `name`, `desc`) VALUES (:serviceId, :name, :desc);";
  $s = $dbh-> prepare ($q);
  $s-> bindParam (':serviceId', $serviceId);

  for ($k=0; $k<count($names); $k++)
  {
    $s-> bindParam (':name', $names[$k]);
    $s-> bindParam (':desc', $descs[$k]);
    $s-> execute();
  }
}
?>
<?php
$q = "SELECT `id`, `name` FROM `product` WHERE `serviceId`=:serviceId;";
$s = $dbh-> prepare ($q);
$s-> bindParam (':serviceId', $serviceId);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$products = $r;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Artist Promotion</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/offcanvas.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      <script>
        var productId = <?php echo $products[0]['id'] ?>;
      </script>
    </head>

    <body>
     <?php 
        include "admin_head.php";
        include "admin_sidepanel.php";

        ?>


    
          <div class="col-xs-12 col-sm-9">

          <div class="row">
            <ul id="products" class="nav nav-tabs">
              <?php foreach ($products as $k=>$product) { ?>
              <li <?php echo "id=\"product_".$product['id']."\"" ?><?php if ($k == 0) echo " class=\" active\"" ?>><a href="#"><?php echo $product['name'] ?></a></li>
              <?php } ?>
            </ul>
            <br>
            <form role="form">
              <div class="form-inline" id="itemRows">
                <div class="form-inline" id="rowNum1">
                  <div class="form-group"><input type="text" id="add_name" name="name[]" class="form-control" placeholder="name of the service"/> </div>
                  <div class="form-group"><input type="text" id="add_cost" name="cost[]" class="form-control" placeholder="cost of the service"/> </div>
                  <div class="form-group"><input type="text" id="add_description" name="desc[]" class="form-control" placeholder="description of the service"/> </div>
                  <div class="form-group"><input type="checkbox" id="optional" name="optional[]" class="form-control"/> </div>
                  <div class="form-group"><button onclick="addRow(this.form);" type="button" class="btn btn-primary form-control" value="Add row">Add rows</button>
                   <br></div>
                 </div>
               </div>



               <button id="itemsSubmit" type="button" class="btn btn-default">Submit</button> <div id="sendingIndicator-sending" style="display: none;">Sending</div> <div id="sendingIndicator-sent" style="display: none;">Sent</div>

             </form>

           </div>
         </div><!--/row-->
       <hr>

     <footer>
      <p>&copy; Company 2013</p>
    </footer>

  </div><!--/.container-->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="offcanvas.js"></script>
    <script>
      $('.dropdown-toggle').dropdown();
    </script>
    <script>

      var rowNum = 1;
      function addRow(frm) {
        rowNum ++;
        var row = '<div class="form-inline" id="rowNum'+rowNum+'"><div class="form-group"><input type="text" name="name[]" value="'+frm.add_name.value+'" class="form-control" placeholder="name of the service"></div><div class="form-group"><input type="text" name="cost[]" value="'+frm.add_cost.value+'" class="form-control" placeholder="cost of the service"></div><div class="form-group"><input type="text" name="desc[]" value="'+frm.add_description.value+'" placeholder="description of the service" class="form-control"></div><div class="form-group"><input type="checkbox" id="optional" name="optional[]" class="form-control"/> </div> <div class="form-group"><button class="btn btn-warning form-control" value="Remove" onclick="removeRow('+rowNum+');">Remove Row</button></div></p></div>';
        jQuery('#itemRows').append(row);
        frm.add_cost.value = '';
        frm.add_name.value = '';
        frm.add_description.value = '';
      }
      function removeRow(rnum) {
        jQuery('#rowNum'+rnum).remove();
      }
    </script>

    <script>
      $('#products li').click( function (e) 
      {
        var id = $(this).attr('id');
        id = id.substr(id.indexOf("_") + 1);

        $('li[id^="product_"]').removeClass ('active');
        $('#product_'+id).addClass ('active');

        productId = id;
      });
    </script>

    <script>
      $("#itemsSubmit").click( function (e)
      {
        var names = [];
        $('[name="name[]"]').each(function()
        {
          names.push($(this).val());
        });
        var costs = [];
        $('[name="cost[]"]').each(function()
        {
          costs.push($(this).val());
        });
        var descs = [];
        $('[name="desc[]"]').each(function()
        {
          descs.push($(this).val());
        });
        var optionals = [];
        $('[name="optional[]"]').each(function()
        {
          if ($(this).is(":checked"))
          	optionals.push('1');
          else
          	optionals.push('0');
        });

        var data = [];
        for (var i in names)
        {
          data[i] = {};
          data[i]['name'] = names[i];
          data[i]['cost'] = costs[i];
          data[i]['desc'] = descs[i];
          data[i]['optional'] = optionals[i];
        }

        var toSend = {};
        toSend['productId'] = productId;
        toSend['data'] = data;

        sendingIndicatorStatus('sending');
        $.post('ajax/addItems.php', toSend, function(data) {sendingIndicatorStatus('complete')});
      });

      function sendingIndicatorStatus(status)
      {
        switch (status)
        {
          case 'sending'  :   $('#sendingIndicator-sent').css('display', 'none');
          $('#sendingIndicator-sending').css('display', 'inline');
          break;
          case 'complete' :   $('#sendingIndicator-sending').fadeOut();
          $('#sendingIndicator-sent').css('display', 'inline');
          break;
        }
      }
    </script>

    <script src="https://raw.github.com/Fa773NM0nK/Fa773N_M0nK-library/master/JavaScript/Get%20URL%20Parameter/getURLParameter.js"></script>
    <script>
    if (getURLParameter('productId') != "")
    {
      $("#product_"+getURLParameter('productId')).trigger("click");
    }
    </script>
  </body>
  </html>

