<?php
require_once ('include/database.php');
?>
<?php
$activeServiceId = $_GET['serviceId'];

$q = "SELECT `id`, `name` FROM `service`;";
$s = $dbh-> prepare ($q);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$allServices = $r;

$inactiveServices = [];
foreach ($r as $val)
{
  if ($val['id'] == $activeServiceId)
    $activeService = $val['name'];
  else
    array_push ($inactiveServices, $val);
}


$q = "SELECT `id`, `name` FROM `product` WHERE `serviceId`=:serviceId;";
$s = $dbh-> prepare ($q);
$s-> bindParam (':serviceId', $activeServiceId);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$products = $r;
$activeProduct = $products[0]['id'];

$questionMarks = str_repeat('?, ', count($r)-1) . '?';
$q = "SELECT * FROM `item` WHERE `productType` IN ($questionMarks);";
$s = $dbh-> prepare ($q);
foreach ($products as $k=>$val)
  $s-> bindParam ($k+1, $val['id']);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$items = $r;
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <style>
    #image
    {
      opacity:0.4;

    }

  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Client Home</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/offcanvas.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      <script>
        var activeProduct = <?php echo $activeProduct; ?>;
        var items = <?php echo json_encode ($items) ?>;
        var products = <?php echo json_encode($products) ?>;
      </script>
    </head>

    <body style="background-image:url('img/bg.png');" >
      <?php

      include "client_head.php";

      ?>
      <div class="container">

        <?php

        include "client_sidepanel.php";

        ?>


        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

          

          <div class="container">

            <div class="row">
              <div class="col-md-7">

                <br>
                  <img src="img/logo/youtube.gif">


                <div id="items">
                 

                </div>

              </div>
            </div><!--/row-->
          </div><!--/span-->

        </div><!--/row-->




        
        <hr>

        <footer>
          <p></p>
        </footer>

      </div><!--/.container-->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="offcanvas.js"></script>
    <script>
      $('.dropdown-toggle').dropdown();
    </script>

    <script>
      $('#products li').click( function (e) 
      {
        var id = $(this).attr('id');
        id = id.substr(id.indexOf("_") + 1);

        $('li[id^="product_"]').removeClass ('active');
        $('#product_'+id).addClass ('active');

        productId = id;

        $('#items').empty();

        var ct = 0;
        appendString = "";
        for (var i in items)
        {
          if (items[i]['productType'] == productId)
          {
            if (ct%3==0)
              appendString += '<div class="row" style="margin-top: 10px; margin-left: 0px; margin-right: 0px">';

            appendString += '<div id="item_'+items[i]['id']+'" class="col-sm-4 col-lg-3';
            if (ct%3!=0) appendString += ' col-lg-offset-1';
            appendString += '" style="border:2px solid green; ">';
            appendString += '<p>'+items[i]['name']+'</p>';
            var productName;
            for(i in products) { if (products[i]['id']==productId) productName=products[i]['name']}
              appendString += '<p>'+productName+'</p>';
            appendString += '<h2>$'+items[i]['cost']+'</h2>';
            appendString += '<p><a class="btn btn-default aPopover" href="#" role="button" data-toggle="popover" data-content="'+items[i]['desc']+'">View details &raquo;</a></p>';
            appendString += '</div>';

            ct ++;

            if ( (ct==items.length) || (ct%3==0) )
            {
              appendString += '</div>';
            }
          }
        }

        $('#items').append(appendString);
        $('#items').hide();
        $('#items').fadeIn();
        $('.aPopover').popover();
        activeProduct = productId;
      });
</script>

<script>
  $('#services li').click( function (e) 
  {
    var id = $(this).attr('id');
    id = id.substr(id.indexOf("_") + 1);

    var oldUrl = window.location.href;

    var newUrl = oldUrl.substr(0, oldUrl.indexOf("=")+1) + id;

    window.location.href = newUrl;
  });
</script>

<script>
  $('.aPopover').popover();
</script>

</body>
</html>

