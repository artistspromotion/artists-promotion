<?php
require_once ('include/database.php');
?>
<?php
if (!isset($_POST['delete']) && !isset ($_POST['serviceIdToDelete']))
{
	if (isset ($_POST['itemId']) )
	{
		$q = "UPDATE `item` SET `cost`=:cost, `name`=:name, `desc`=:desc WHERE `id`=:itemId;";
		$s = $dbh-> prepare ($q);
		$s-> bindParam (":cost", $_POST['cost']);
		$s-> bindParam (":name", $_POST['name']);
		$s-> bindParam (":desc", $_POST['desc']);
		$s-> bindParam (":itemId", $_POST['itemId']);
		$s-> execute ();
	}
	elseif (isset ($_POST['productId']) )
	{
		$q = "UPDATE `product` SET `name`=:name WHERE `id`=:productId;";
		$s = $dbh-> prepare ($q);
		$s-> bindParam (":name", $_POST['name']);
		$s-> bindParam (":productId", $_POST['productId']);
		$s-> execute ();
	}
}
elseif (isset ($_POST['serviceIdToDelete']))
{
	$q = "DELETE FROM `service` WHERE `id`=:serviceId;";
	$s = $dbh-> prepare ($q);
	$s-> bindParam (":serviceId", $_POST['serviceIdToDelete']);
	$s-> execute ();

	array_map('unlink', glob('img/serviceImage/'.$_POST['serviceName'].'.*'));
	array_map('unlink', glob('img/serviceIcon/'.$_POST['serviceName'].'.*'));
}
else
{
	if (isset ($_POST['itemId']) )
	{
		$q = "DELETE FROM `item` WHERE `id`=:itemId;";
		$s = $dbh-> prepare ($q);
		$s-> bindParam (":itemId", $_POST['itemId']);
		$s-> execute ();
	}
	elseif (isset ($_POST['productId']) )
	{
		$q = "DELETE FROM `product` WHERE `id`=:productId;";
		$s = $dbh-> prepare ($q);
		$s-> bindParam (":productId", $_POST['productId']);
		$s-> execute ();
	}
}

header('Location: ' . $_SERVER['HTTP_REFERER']);
?>