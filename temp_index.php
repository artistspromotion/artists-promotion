<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Artist Promotion</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
<style>
/* Lastly, apply responsive CSS fixes as necessary */
      
.bs-foote.affix {
    position: fixed;
    top: 80%;
}
      @media (max-width: 767px) {
        #foot {
          margin-left: -20px;
          margin-right: -20px;
          padding-left: 20px;
          padding-right: 20px;
        }
      }


</style>
      <!-- Custom styles for this template -->
      <link rel="stylesheet" href="css/layout.css" type="text/css" />

      <link href="css/carousel.css" rel="stylesheet">
    </head>
<!-- NAVBAR
  ================================================== -->
  <body>

    <!-- Static navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top" style="background:rgba(102,0,102,0.7);" role="navigation">
      <div class="container">
        <div class="header">

          <ul class="nav navbar-nav ">
           <li><a href="#" style="color:rgb(255,255,255); font-size:18px;">
            <span class="glyphicon glyphicon-headphones"></span>
            Artist Promotion

          </a></li>



          <li><a href="#" style="color:rgb(255,255,0); font-size:18px; text-alig:center">
            <span class="glyphicon glyphicon-music"></span>  Music</a></li>
            <li><a href="#" style="color:rgb(255,51,153); font-size:18px;"><span class="glyphicon glyphicon-facetime-video"></span> Video</a></li>
            <li><a href="#" style="color:rgb(0,255,128); font-size:18px;"><span class="glyphicon glyphicon-user"></span> Social-Media</a></li>
          </ul>      


          <ul class="nav navbar-nav pull-right">
           <li><a href="#"style="color:rgb(255,178,102); font-size:16px; text-alig:center"><span class="glyphicon glyphicon-star"></span> About Us</a></li>
           <li><a href="#signup" data-toggle="modal"style="color:rgb(224,224,224); font-size:16px; text-alig:center"><span class="glyphicon glyphicon-thumbs-up"></span> Signup</a></li>
           <li><a href="#login" data-toggle="modal" style="color:cyan; font-size:16px; text-alig:center"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
         </ul>      
       </h3>
     </div>


<!--<ul class="nav navbar-nav pull-right">
 
<li><h3>Artist promotiom</h3></li>
 <li><a href="#">About Us</a></li>
            <li><a href="#">Signup</a></li>
            <li><a href="#">Login</a></li>
          </ul>        --> 
        </div><!--/.navbar-collapse -->


      </div>
    </div>






    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="img/v.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption" style="background:;">
              <h1>Videos.</h1>
              <p>Your awesome videos are just a step away from getting famous. Check out our attractive plans we offer you.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="img/v.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Music.</h1>
              <p>Your music is just one step away from topping the charts. Make yourself heard by all.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="img/v.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Social Media.</h1>
              <p>Need someone to explore your social media site.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>

    </div><!-- /.carousel -->

    <div class="modal fade" id="signup" style="background:rgba(102,0,204,0.1);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


      <div class="modal-dialog" >
        <div class="modal-content">
          <div class="modal-header" style="background:rgba(0,51,102,1);">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel" style="color:white;">SignUp</h4>
          </div>
          <div class="modal-body" style="background:rgba(0,51,102,.0);">
           <?php
           if(isset($_GET['message']))
           {
            ?>
            <div class="alert alert-success alert-block">
             <a class="close" data-dismiss="alert" href="#">&times;</a>
             <h4 class="alert-heading"></h4>

             <?php
             echo $_GET['message'];
             ?>
           </div>
           <?php
           
         }

         ?>

         
         <!--   Sign up form   -->
         <form role="form" method="POST" class="form-horizontal" action="signup_verify.php" enctype="multipart/form-data">
          <div class="form-group" >
           <label for="exampleInputEmail1" class="col-sm-4 control-label">Full Name</label>
           <div class="col-sm-7">
             <input type="text" class="form-control" id="exampleInputEmail1" name="register_name" placeholder="Enter Full Name" required>
           </div>

         </div>

         <div class="form-group">
           <label for="exampleInputEmail1"class="col-sm-4 control-label">Email address</label>
           <div class="col-sm-7">
             <input type="email" class="form-control" id="exampleInputEmail1" name="register_email" placeholder="Enter email" required>
           </div>

         </div>
         <div class="form-group">
           <label for="exampleInputPassword1" class="col-sm-4 control-label">Password</label>
           <div class="col-sm-7">
             <input type="password" class="form-control" id="exampleInputPassword1" name="register_password" placeholder="Password" required>
           </div>

         </div>
         <div class="form-group">
           <label for="exampleInputPassword1" class="col-sm-4 control-label">Re-Enter Password</label>
           <div class="col-sm-7">
             <input type="password" class="form-control" id="exampleInputPassword1" name="register_re_password" placeholder="Password" required>
           </div>

         </div>
         <hr>
         <div class="form-actions" style="text-align:center;">
           <button type="submit" class="btn btn-primary">Submit</button>
           <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>

         </div>
       </form>
       <div class="modal-footer">
       </div>


     </div> 
   </div><!-- /.modal-content -->
 </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" style="background:rgba(102,0,204,0.1);" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"style="background:rgba(0,51,102,1);">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel" style="color:white;">Login</h4>
      </div>
      <div class="modal-body" style="background:rgba(0,51,102,.0);">
       <?php 
       if(isset($_GET['info']))
       {
         ?>
         <div class="alert alert-warning alert-block">
           <a class="close" data-dismiss="alert" href="#">&times;</a>
           <h4 class="alert-heading"></h4>
           <?php echo $_GET['info'];?>

         </div>
         <?php
       }
       ?>


       <!--   Login form   -->

       <form role="form" method="post" class="form-horizontal" action="login_user.php" enctype="multipart/form-data">


        <div class="form-group">
         <label for="exampleInputEmail1"class="col-sm-4 control-label">Email address</label>
         <div class="col-sm-7"><input type="email" class="form-control" id="exampleInputEmail1" name="register_email" placeholder="Enter email" required>
         </div>
       </div>
       <div class="form-group">
         <label for="exampleInputPassword1"class="col-sm-4 control-label">Password</label>
         <div class="col-sm-7"><input type="password" class="form-control" id="exampleInputPassword1" name="register_password" placeholder="Password" required>
         </div>
       </div>

       <hr>
       <div class="form-actions"style="text-align:center;">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </form>
    <div class="modal-footer">
    <a data-dismiss="modal" data-toggle="modal" href="#forgot_password" style="color:; font-size:16px;"><center>Forgot Password?</a>
        </div>

    
  </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" style="background:rgba(102,0,204,0.1);" id="forgot_password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  

  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"style="background:rgba(0,51,102,1);">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel" style="color:white;">Forgot Password</h4>
      </div>
      <div class="modal-body" style="background:rgba(0,51,102,.0);">
       

       <!--   Forgot Password form   -->

       <form role="form" method="post" class="form-horizontal" action="forgot_password.php" enctype="multipart/form-data">


        <div class="form-group">
         <label for="exampleInputEmail1"class="col-sm-4 control-label">Email address</label>
         <div class="col-sm-7"><input type="email" class="form-control" id="exampleInputEmail1" name="register_email" placeholder="Enter email" required>
         </div>
       </div>
       
       <hr>
       <div class="form-actions"style="text-align:center;">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </form>
    <div class="modal-footer">
    </div>

    
  </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="jumbotron" style="margin-top:-60px;">
  <h2 style="margin-top:-20px; text-align:center">You are just a step away from getting famous. </h2>
  <form class="form-inline" role="form">
    <div class="row" style="align:center;">
      <div class="col-xs-12 col-md-12" style="align:center;">
        <center>Enter a You Tube video link and see how the trick is done for you.
      </div>
<div class="col-xs-5 col-md-2" style="align:center;">
      </div>
      <div class="col-xs-12 col-md-7" style="align:center;">
        <input type="email" class="form-control input-lg" id="exampleInputEmail2" placeholder="http://www.youtube.com/watch?v=dFBzoMPiyI8" value="http://www.youtube.com/watch?v=dFBzoMPiyI8">
      </div>  
      <div class="col-xs-1"><button type="submit" class="btn btn-success btn-lg">Go !!</button></div>

    </div>
  </form>



<!--<div class="row">
<div class="col-xs-7 col-md-1"><img src="img/logo/youtube.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/yahooscreen.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/vimeo.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/vube.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/netflix.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/ustream.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/tv.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/metacafe.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/liveleak.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/justintv.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/hulu.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/filmon.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/crackle.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/dailymotion.gif"></div>
<div class="col-xs-7 col-md-1"><img src="img/logo/break.gif"></div>

</div>
-->
</div>    <!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container marketing">

  <!-- Three columns of text below the carousel -->
  <div class="row">
    <div class="col-lg-4">
      <center>
        <img class="img-circle" src="img/twitter.jpeg" alt="Generic placeholder image">
        <h2>Twitter</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
        <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4">
        <center>
          <img class="img-circle" src="img/youtube.jpg" alt="Generic placeholder image">
          <h2>Youtube</h2>
          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <center>
            <img class="img-circle" src="img/facebook.png" alt="Generic placeholder image">
            <h2>Facebook</h2>
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
          </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->


        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="row featurette">
          <div class="col-md-7">
            <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
          </div>
          <div class="col-md-5">
            <img class="featurette-image img-responsive" src="img/500.png" alt="Generic placeholder image">
          </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
          <div class="col-md-5">
            <img class="featurette-image img-responsive" src="img/500.png" alt="Generic placeholder image">
          </div>
          <div class="col-md-7">
            <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
          </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
          <div class="col-md-7">
            <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
          </div>
          <div class="col-md-5">
            <img class="featurette-image img-responsive" src="img/500.png" alt="Generic placeholder image">
          </div>
        </div>

        <hr>

        <!-- /END THE FEATURETTES -->

            <div class="container">
            <div class="row">
<h3 align="center">TESTIMONIALS</h1>
            <div class="col-md-5">
<h5>         "Execellent website, with a very hig efficiency . Delivered all the product on time." 
         <p align="right">Osazee Samson</p>

<br>
       "Execellent website, with a very hig efficiency . Delivered all the product on time." 
         <p align="right">Osazee Samson</p>

            </div>

<div class="col-md-1">

            </div>
<div class="col-md-6">
   <h5><a href="#">
     Twitter</a>: the feed of activities goin on the site a particular intant of time.
        



            </div>




            </div>

<div class="bs-foote affix" >
      <div class="col-md-2">
      </div>

      <div class="alert alert-info col-md-9 fade in"style="background:rgb(69,5,69); color:white">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4 style="text:white;">Promotions offers discount!</h4>
        <p>Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
        <p>
          <button type="button" class="btn btn-danger">Take this action</button>
          <button type="button" class="btn btn-default">Or do this</button>
        </p>
      </div>
<div class="col-md-2">
</div>

   </div><!-- /example -->





<hr class="featurette-divider">
        <!-- FOOTER -->
        <footer style="background:rgba(96,96,96,.4);">
        <div class="container">
        <div class="row">
<div class="col-md-3">

<h3>Links</h3>
<h5>Home <br>About Us <br>FAQ <br>Client portal <br>Tour <br>Compare us <br>Testimonial <br>Blog <br>Contest


</div>
<div class="col-md-3">
<h3>Latest Tweets</h3>
</div>
<div class="col-md-3">
<h3>Connect With Us</h3>
<h5>Facebook<br>Twitter<br>Google +<br>Pinterest<br>Email</div>
<div class="col-md-3">
<h3>Trusted By</h3>
<h5>Verisign<br>Go Daddy <br> DMRC

</div>
</div>
</div>
        </footer>

      </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/holder.js"></script>
    <?php
    if(isset($_GET['message']))
    {
      ?>
      <script>
        $('#signup').modal('show');
      </script>
      <?php
    }
    else
      if(isset($_GET['info']))
      {
        ?>
        
        <script>
        $('#login').modal('show');
        </script>
        <?php
      }
      ?>
    </body>
    </html>

