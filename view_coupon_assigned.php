<?php
require_once ('include/database.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <style>
    #image
    {
      opacity:0.4;

    }

  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Client Home</title>

  <!-- Bootstrap core CSS -->

  <!-- Custom styles for this template -->

        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    

<link href="css/nav.css" rel="stylesheet">
  
  <link href="css/bootstrap.css" rel="stylesheet">
  
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      <script>
        var activeProduct = <?php echo $activeProduct; ?>;
        var items = <?php echo json_encode ($items) ?>;
        var products = <?php echo json_encode($products) ?>;
      </script>

    </head>

    <body>
      <?php

      include "admin_head.php";

      ?>
      <div class="container">

        <?php

        include "admin_sidepanel.php";

        ?>


        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

          <div class="row-fluid">
                        <!-- block -->
                        <div class="masthead">
        <ul class="nav nav-justified">
          <li ><a href="view_coupon.php" >Unassigned Coupons</a></li>
          <li class="active"><a href="view_coupon_assigned.php">Assigned Coupons</a></li>
        </ul>
      </div>
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Coupons Assigned</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span11">
 <form action="view_coupon_backend.php" method="post">
<tr><td colspan="4">
<input type="submit" name="submit" value="Assign" disabled>
<input type="submit" name="submit" value="Unassign" >

<input type="submit" name="submit" value="Delete" >
</td>
</tr>
 <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                        


                        <thead>
                            <tr>
                            <Th>Coupon Id</Th>
<th>Coupon Value</th>
<th>Minimum Amount</th>
<th>Coupon Type</th>
<th>Coupon Time</th>
<th>Coupon Status</th>
                               <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                           <?php
					$sql="select * from coupons where coupon_status='Assigned';";
					$stmt=$dbh->prepare($sql);
					$stmt->execute();
					?>

					<?php


while($r =$stmt->fetch())
{
  echo "<tr>";
echo "<td>".$r['coupon_id']."</td>";
echo "<td>".$r['coupon_value']."</td>";
echo "<td>".$r['min_amount']."</td>";
echo "<td>".$r['coupon_type']."</td>";
echo "<td>".$r['coupon_time']."</td>";
echo "<td>".$r['coupon_status']."</td>";
$id1=$r['id'];



?>
<td><input type="checkbox" value="<?php echo $id1;?>" name="check[]">
</td>
</tr>



<?php



}

?>



                        </tbody>
                    </table>
                    </td>
                    </form>

</div>
</div>
</div>
                


                 
                 
              </div >          
            </div>

          </div><!--/container-->
<!-- update modal -->
         
         
<hr>

<footer>
  <p>&copy; Company 2013</p>
</footer>

</div><!--/.container-->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      $('.dropdown-toggle').dropdown();
    </script>

  
 <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>
        <script src="assets/DT_bootstrap.js"></script>

<script>
 $('.aPopover').popover();
</script>

