<?php
require_once 'session.php';
?>
<?php
require_once ('include/database.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <style>
    #image
    {
      opacity:0.4;

    }

  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Client Home</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/offcanvas.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
<!--
      <script>
        var activeProduct = <?php echo $activeProduct; ?>;
        var items = <?php echo json_encode ($items) ?>;
        var products = <?php echo json_encode($products) ?>;
      </script>
-->
    </head>

    <body >
      <?php

      include "client_head.php";

      ?>
      <div class="container">
        <?php

        include "client_sidepanel.php";

        ?>


        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

          

          <div class="container">

            <div class="row">
<div class="col-sm-9">
                 
             <h3 style="color:purple">Fund your account today for hassle free transactions
              </h3><p>
             For your convenience, we have set up two options for you to buy our service packages. You can pay directly using Paypal/Credit Card or Fund your account for hassle free transactions.
             </p>
             <hr>
        <center>

<form id="payment1" class="form-horizontal" role="form">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Enter Amount</label>
    <div class="col-sm-10">
    <input type="hidden" name="item_name_1" value="Add Fund">
      <input type="text" class="form-control" id="fund_amount" name="amount_1" placeholder="Amount">


    </div>
  </div>
<button onclick="submit_form(this.id)" id="paypal" name="submit" class="btn btn-success">Proceed to paypal</button>
<button onclick="submit_form(this.id)" id="checkout2" name="submit" class="btn btn-success">Proceed to 2checkout</button>
</form>

    </center>

              </div>
            </div><!--/row-->
          </div><!--/span-->

   
</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="offcanvas.js"></script>
    <script>
      $('.dropdown-toggle').dropdown();
    </script>
    <script>
      function submit_form(id)
      {
        if(id=="paypal")
        {
          alert(id);
          $('#payment1').append("<input type='hidden' name='cmd' value='_cart'>");
          $('#payment1').append("<input type='hidden' name='business' value='avsrocks19@gmail.com'>");
          $('#payment1').append("<input type='hidden' name='upload' value='1'>");
          $('#payment1').append("<input type='hidden' name='rm' value='2'>");
          $('#payment1').append("<input type='hidden' name='return' value='http://127.0.0.1/xampp/artists-promotion/transaction_successfull.php'>");
          $('#payment1').attr('action','https://www.sandbox.paypal.com/cgi-bin/webscr');
          $('#payment1').submit();
        }
        else
          if(id=="checkout2")
        {
          alert(id);
          $('#payment1').append("<input type='hidden' name='sid' value='202200392'>");
          $('#payment1').append("<input type='hidden' name='mode' value='2CO'>");
          $('#payment1').append("<input type='hidden' name='li_0_type' value='product'>");
          $('#payment1').append("<input type='hidden' name='li_0_product_id' value='Fund'>");
          $('#payment1').append("<input type='hidden' name='li_0_name' value='Fund'>");
          $('#payment1').attr('action','https://www.2checkout.com/checkout/purchase');
          $('#fund_amount').attr('name','li_0_price');
          $('#payment1').submit();
        }


      }
    </script

   

</body>
</html>

