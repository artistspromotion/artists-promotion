<?php

	include "admin_head.php";
	include "admin_sidepanel.php";

?>
		

<?php
require_once ('include/database.php');
?>

<?php
$title = $_POST['title'];
$text = $_POST['text'];
$type = $_POST['type'];
$productId = $_POST['productId'];

if ($_FILES['image']['error'] == UPLOAD_ERR_NO_FILE)
{
	$imageName = NULL;
}
else
{
	if ( !$imageInfo = getimagesize($_FILES['image']['tmp_name']) )
	{
	  echo "FATAL ERROR!";
	  exit;
	}
	$imageExtention = substr($imageInfo['mime'], strpos($imageInfo['mime'], "/") + 1);
	$imageName = "";
}

$q = "INSERT INTO `notification` (`title`, `text`, `type`, `productId`) VALUES (:title, :text, :type, :productId);";
$s = $dbh-> prepare ($q);
$s-> bindParam(':title', $title);
$s-> bindParam(':text', $text);
$s-> bindParam(':type', $type);
$s-> bindParam(':productId', $productId);
$s-> execute();

if ($imageName !== NULL)
{
	$notificationId = $dbh->lastInsertId();

	$imageName = 'notification_'.$notificationId.'.'.$imageExtention;
	move_uploaded_file ($_FILES['image']['tmp_name'] , "img/notificationImage/".$imageName);

	$q = "UPDATE `notification` SET `image`=:image WHERE `id`=:id;";
	$s = $dbh-> prepare ($q);
	$s-> bindParam(':image', $imageName);
	$s-> bindParam(':id', $notificationId);
	$s-> execute();
}

echo "Notification Generated Successfuly!";
?>

