<?php
require_once ('include/database.php');
?>
<?php
$adminId = 1; // Was told that admin's user Id would always be 1. ** CAN ALSO TAKE THIS FROM $_SESSION **
$userId = $_GET['to'];

$q = "SELECT * FROM `message` WHERE (`from`=:userId AND `to`=:adminId) OR (`from`=:adminId AND `to`=:userId);";
$s = $dbh-> prepare ($q);
$s-> bindParam (':userId', $userId);
$s-> bindParam (':adminId', $adminId);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$messages = $r;

$q = "SELECT `user_name` FROM `users` WHERE `user_id`=:userId;";
$s = $dbh-> prepare ($q);
$s-> bindParam (':userId', $userId);
$s-> execute();
$r = $s-> fetch(PDO::FETCH_ASSOC);

$userName = $r['user_name'];
?>
<?php
$q_1 = "UPDATE `message` SET `status`=1 WHERE `id`=:messageId;";
$s_1 = $dbh-> prepare ($q_1);
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <style>
    #image
    {
      opacity:0.4;

    }

  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Messages</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/offcanvas.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      <script>
        var activeProduct = <?php echo $activeProduct; ?>;
        var items = <?php echo json_encode ($items) ?>;
        var products = <?php echo json_encode($products) ?>;
      </script>
    </head>

    <body style="background-image:url('img/bg.png');" >
      <?php

      include "admin_head.php";

      ?>
      <div class="container">

        <?php

        include "admin_sidepanel.php";

        ?>


        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

          

          <div class="container">

            <div class="row">
              <div class="col-md-7">

                <br>

                <div class="row">
                  <div class="col-sm-4">
                    <b><?php echo $userName ?></b>
                  </div>
                  <div class="col-sm-4 pull-right text-right">
                    <b>Me</b>
                  </div>
                </div>

                <hr>

                <?php
                foreach ($messages as $message)
                {
                  if ($message['from'] == $userId)
                  {
                    ?>
                    <div class="row">
                      <div class="col-sm-11" style="color:green   " <?php if ($message['status']==0) { echo " newMessage"; }?>">
                        <?php echo $message['message'] ?>
                        <?php
                        $s_1-> bindParam (':messageId', $message['id']);
                        $s_1-> execute();
                        ?>
                      </div>
                    </div>
                    <?php
                  }
                  else // from admin
                  {
                    ?>
                    <div class="row">
                      <div class="col-sm-offset-1 col-sm-11 text-right" style="color:red">
                        <?php echo $message['message'] ?>
                      </div>
                    </div>
                    <?php
                  }
                }
                ?>                

                <br>

                <div class="col-sm-12">
                  <form action="submitMessage_admin.php" method="POST">
                    <input name="to" type="hidden" value="<?php echo $userId ?>">
                    <textarea name="message" rows="5" style="width: 100%"></textarea>
                    <br><br>
                    <button class="btn btn-primary pull-right" type="submit">Submit</button>
                  </form>
                </div>

              </div>
            </div><!--/row-->
          </div><!--/span-->

        </div><!--/row-->




        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

          <div class="header">
            <div class="navbar navbar-right">

              <!-- <h3>Balance = 100 $</h3> -->


              <!-- <button type="button" class="btn btn-primary btn-lg">Check Out</button>   -->
            </div >          
          </div>

        </div><!--/container-->
        <hr>

        <footer>
          <p>&copy; Company 2013</p>
        </footer>

      </div><!--/.container-->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="offcanvas.js"></script>
    <script>
      $('.dropdown-toggle').dropdown();
    </script>

    <script>
      $('#products li').click( function (e) 
      {
        var id = $(this).attr('id');
        id = id.substr(id.indexOf("_") + 1);

        $('li[id^="product_"]').removeClass ('active');
        $('#product_'+id).addClass ('active');

        productId = id;

        $('#items').empty();

        var ct = 0;
        appendString = "";
        for (var i in items)
        {
          if (items[i]['productType'] == productId)
          {
            if (ct%3==0)
              appendString += '<div class="row" style="margin-top: 10px; margin-left: 0px; margin-right: 0px">';

            appendString += '<div id="item_'+items[i]['id']+'" class="col-sm-4 col-lg-3';
            if (ct%3!=0) appendString += ' col-lg-offset-1';
            appendString += '" style="border:2px solid green; ">';
            appendString += '<p>'+items[i]['name']+'</p>';
            var productName;
            for(i in products) { if (products[i]['id']==productId) productName=products[i]['name']}
            appendString += '<p>'+productName+'</p>';
            appendString += '<h2>$'+items[i]['cost']+'</h2>';
            appendString += '<p><a class="btn btn-default aPopover" href="#" role="button" data-toggle="popover" data-content="'+items[i]['desc']+'">View details &raquo;</a></p>';
            appendString += '</div>';

            ct ++;

            if ( (ct==items.length) || (ct%3==0) )
            {
              appendString += '</div>';
            }
          }
        }

        $('#items').append(appendString);
        $('#items').hide();
        $('#items').fadeIn();
        $('.aPopover').popover();
        activeProduct = productId;
      });
</script>

<script>
  $('#services li').click( function (e) 
  {
    var id = $(this).attr('id');
    id = id.substr(id.indexOf("_") + 1);

    var oldUrl = window.location.href;

    var newUrl = oldUrl.substr(0, oldUrl.indexOf("=")+1) + id;

    window.location.href = newUrl;
  });
</script>

<script>
  $('.aPopover').popover();
</script>

</body>
</html>

