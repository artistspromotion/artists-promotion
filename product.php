<?php
require_once ('include/database.php');
?>
<?php
if (!isset ($_POST['serviceId']) )
{
  $type = $_POST['type'];
  $name = $_POST['name'];

  if ( !$imageInfo = getimagesize($_FILES['image']['tmp_name']) )
  {
    echo "FATAL ERROR!";
    exit;
  }
  $imageExtention = substr($imageInfo['mime'], strpos($imageInfo['mime'], "/") + 1);
  $imageFilename = $name.'.'.$imageExtention;
  move_uploaded_file ($_FILES['image']['tmp_name'] , "img/serviceImage/".$imageFilename);

  if ( !$iconInfo = getimagesize($_FILES['icon']['tmp_name']) )
  {
    echo "FATAL ERROR!";
    exit;
  }
  $iconExtention = substr($iconInfo['mime'], strpos($iconInfo['mime'], "/") + 1);
  $iconFilename = $name.'.'.$iconExtention;
  move_uploaded_file ($_FILES['icon']['tmp_name'] , "img/serviceIcon/".$iconFilename);

  $q = "INSERT INTO `service` (`name`, `typeId`, `imageFilename`, `iconFilename`) VALUES (:name, :type, :imageFilename, :iconFilename);";
  $s = $dbh-> prepare ($q);
  $s-> bindParam(':name', $name);
  $s-> bindParam(':type', $type);
  $s-> bindParam(':imageFilename', $imageFilename);
  $s-> bindParam(':iconFilename', $iconFilename);
  $s-> execute();

  $serviceId = $dbh->lastInsertId();
}
else
{
  $serviceId = $_POST['serviceId'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Artist Promotion</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/offcanvas.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

    </head>

    <body >
            <?php 
        include "admin_head.php";
        include "admin_sidepanel.php";

        ?>

          
          
<div class="col-xs-12 col-sm-9">
          
          <div class="row">
            <form role="form" action="item.php" method="POST">
              <input type="hidden" name="serviceId" value="<?php echo $serviceId ?>">
              <div class="form-inline" id="itemRows">
                <div class="form-group"><label for="add_name">Item name:</label></div>
                <div class="form-group"><input type="text" id="add_name" name="name[]" class="form-control" placeholder="Eg: Likes, Dislikes"/> </div>
                <div class="form-group"><input type="text" id="add_description" name="desc[]" class="form-control" placeholder="general description"/> </div>
                <div class="form-group"><button onclick="addRow(this.form);" type="button" class="btn btn-primary form-control" value="Add row">Add rows</button>
                </div>



              </div>
              <button type="submit" class="btn btn-default">Submit</button>




            </form>

          </div>
  </div>

        <hr>

    <footer>
      <p>&copy; Company 2013</p>
    </footer>

  </div><!--/.container-->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="offcanvas.js"></script>
    <script>
      $('.dropdown-toggle').dropdown();
    </script>
    <script>

      var rowNum = 0;
      function addRow(frm) {
        rowNum ++;
        var row = '<div class="form-inline" id="rowNum'+rowNum+'"><div class="form-group"><label for="add_name">Item name:</label></div><div class="form-group"><input type="text" name="name[]" value="'+frm.add_name.value+'" class="form-control" placeholder="Eg: Likes, Dislikes"></div><div class="form-group"><input type="text" name="desc[]" value="'+frm.add_description.value+'" class="form-control" placeholder="general description"></div> <div class="form-group"><button class="btn btn-warning form-control" value="Remove" onclick="removeRow('+rowNum+');">Remove Row</button></div></p></div>';
        jQuery('#itemRows').append(row);
        frm.add_description.value = '';
        frm.add_name.value = '';
      }
      function removeRow(rnum) {
        jQuery('#rowNum'+rnum).remove();
      }
    </script>


  </body>
  </html>

