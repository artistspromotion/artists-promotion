<?php
require_once ('include/database.php');
?>
<?php
$q = "SELECT * FROM `notification`;";
$s = $dbh-> prepare($q);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$allNotifications = $r;
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <style>
    #image
    {
      opacity:0.4;

    }

  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Manage Notification</title>

  <!-- Bootstrap core CSS -->

  <!-- Custom styles for this template -->

        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    

<link href="css/nav.css" rel="stylesheet">
  
  <link href="css/bootstrap.css" rel="stylesheet">
  
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

    </head>

    <body>
      <?php

      include "admin_head.php";

      ?>
      <div class="container">

        <?php

        include "admin_sidepanel.php";

        ?>


        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

<div class="row">
<table class="table table-striped table-bordered">
  <thead>
    <th>Title</th>
    <th>Text</th>
    <th>Type</th>
    <th>Image</th>
    <th>Related Product</th>
    <th>Edit</th>
    <th>Delete</th>
  </thead>

  <tbody>
  <?php foreach ($allNotifications as $notification) { ?>
    <tr id="<?php echo $notification['id'] ?>">
      <td class="title"><?php echo $notification['title'] ?></td>
      <td class="text"><?php echo $notification['text'] ?></td>
      <td class="type"><?php echo $notification['type'] == 0 ?  "General" : $notification['type'] == 1 ?  "Top Bar" : "Toastr" ?></td>
      <td class="image"><?php echo $notification['image'] ?></td>
      <td class="productId"><?php echo $notification['productId'] ?></td>
      <td style="text-align: center;" class="notificationEdit"><span class="glyphicon glyphicon-pencil"></span></td>
      <td style="text-align: center;" class="notificationDelete"><span class="glyphicon glyphicon-remove"></span></td>
    </tr>
  <?php } ?>
  </tbody>
</table>
</div>

<div id="editNotificationModal" class="modal fade">
  <div class="modal-dialog" style="width: 90%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Edit Notification</h4>
      </div>
      <form id="modifyNotification" action="modify_notification.php" method="POST" class="form-horizontal" role="form">
        <div class="modal-body">

          <input type="hidden" id="id" name="id">

          <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title : </label>
            <div class="col-sm-10">
              <input id="title" name="title" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label for="text" class="col-sm-2 control-label">Text : </label>
            <div class="col-sm-10">
              <textarea id="text" name="text" class="form-control" cols="80" rows="10"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<form id="notificationDelete" action="modify_notification.php" method="POST">
  <input id="notificationDeleteId" name="notificationDeleteId" type="hidden">
</form>

<hr>

<footer>
  <p>&copy; Company 2013</p>
</footer>

</div><!--/.container-->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      $('.dropdown-toggle').dropdown();
    </script>

  
 <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>
        <script src="assets/DT_bootstrap.js"></script>

<script>
 $('.aPopover').popover();
</script>

<script>
$('.notificationDelete').click (function ()
{
  var id = $(this).parent().attr('id');

  $('#notificationDeleteId').attr ('value', id);
  $('#notificationDelete').submit ();
});

$('.notificationEdit').click (function ()
{
  $('#editNotificationModal').modal('show');

  var id = $(this).parent().attr('id');
  var title = $(this).parent().find('.title').html();
  var text = $(this).parent().find('.text').html();
  $('#id').attr ('value', id);
  $('#title').attr ('placeholder', title);
  $('#text').attr ('placeholder', text);
});

$('#modifyNotification').submit (function ()
{
  if ($('#title').val() == "")
    $('#title').val($('#title').attr('placeholder'));
  if ($('#text').val() == "")
    $('#text').val($('#text').attr('placeholder'));

  return true;
});
</script>

</body>
</html>