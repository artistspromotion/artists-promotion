<?php 
//print_r($_POST);
?>
<?php
require_once 'session.php';
require_once ('include/database.php');
if(isset($_SESSION['discount']))
{

  $disc=$_SESSION['discount'];

}
else
  $disc="Nill";
$i=0;
$num=0;
while(isset($_POST['li_'.$i.'_type']))
{
  if($_POST['li_'.$i.'_type']=="product")
  {
    
    $num++;
    $i++;
  }
  else
    break;


}
$gr=$_POST['total'];
$i=0;
if(!isset($_SESSION['final']) ||$gr==$_SESSION['final'])//23 represents gross amount
{ $txn_id=$_POST['invoice_id'];
$sql="insert into `transaction_id`(`tx_id`,mode,discount_code,total) values(:txn_id,'2checkout',:discount,:total);";
$stmt=$dbh->prepare($sql);
$stmt->bindParam (':txn_id', $txn_id);
$stmt->bindParam (':discount', $disc);
$stmt->bindParam (':total', $gr);
$user_id=$_SESSION['id'];
if($stmt->execute())
{
  while($i<$num)
  { 


    $txn_id=$_POST['invoice_id'];
    $item_id=$_POST['li_'.$i.'_name'];
    $item_gross=$_POST['li_'.$i.'_price'];
    $payment_status=$_POST['credit_card_processed'];
    $first_name=$_POST['first_name'];
    $last_name=$_POST['last_name'];
    $payer_email=$_POST['email'];



    $sql="insert into transactions(txn_id,item_id,item_gross,payment_status,first_name,last_name,payer_email,user_id) values(:txn_id,:item_id,:item_gross,:payment_status,:first_name,:last_name,:payer_email,:user_id);";
    $stmt=$dbh->prepare($sql);
    $stmt->bindParam (':txn_id', $txn_id);
    $stmt->bindParam (':item_id', $item_id);
    $stmt->bindParam (':item_gross',$item_gross);
    $stmt->bindParam (':payment_status', $payment_status);
    $stmt->bindParam (':first_name', $first_name);
    $stmt->bindParam (':last_name', $last_name);
    $stmt->bindParam (':payer_email', $payer_email);
    $stmt->bindParam (':user_id', $user_id);

    if($stmt->execute())
    {

      $x=1;

    }
    else
      $x=2;
    $i++;
  }
} 
else
  $x=3;

}
else
  $x=4;
//
// A very simple PHP example that sends a HTTP POST to a remote site
//

//print_r( $_POST);
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <style>
    #image
    {
      opacity:0.4;

    }

  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Client Home</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/offcanvas.css" rel="stylesheet">

</head>

<body >
  <?php

  include "client_head.php";

  ?>
  <div class="container">
    <?php

    include "client_sidepanel.php";

    ?>


    <div class="col-xs-12 col-sm-9">
      <p class="pull-right visible-xs">
        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
      </p>



      <div class="container">

        <div class="row">
          <div class="col-md-9">
            <?php if($x==1)
            {

              ?>

              <h1 style="color:brown">Congratulations, </h1><p><h3>Your transaction was a smashing success !!</h3> 
            </p>
            <table class="table">
              <tr><td>Transaction Details</td></tr>
              <tr><th>Transaction Id</th><th>Item Id</th><th>Item Name</th><th>Item Cost</th><th>Payment Status</th></tr>
              <?php 
              $i=0;
              while($i<$num)
              { 
                $txn_id=$_POST['invoice_id'];
                $item_id=$_POST['li_'.$i.'_name'];
                $a=explode('#', $item_id);
                $item_gross=$_POST['li_'.$i.'_price'];
                $payment_status=$_POST['credit_card_processed'];
                $first_name=$_POST['first_name'];
                $last_name=$_POST['last_name'];
                $payer_email=$_POST['email'];
                $stmt=$dbh->prepare("select * from item where id=:item_id");
                $stmt->bindParam(':item_id',$a[1]);
                $stmt->execute();
                $r=$stmt->fetch();

                ?>
                <tr>
                  <td><?php echo htmlentities($txn_id);?></td>
                  <td><?php echo htmlentities($item_id);?></td>
                  <td><?php echo htmlentities($r['name']);?></td>
                  <td><?php echo htmlentities($item_gross);?></td>
                  <td><?php echo htmlentities($payment_status);?></td>
                </tr>
                <?php
                $i++;
              }
              if($_SESSION['discount'])
              {
                ?>

                <tr><th>Coupon Applied:</th> <td><?php echo $_SESSION['discount'];?></td></tr>
                <tr><th>Total Amount:</th> <td><?php echo $gr;?></td>
                </table>
                <?php
              }
            }
            else
              if($x==2)
              {
               ?>
               <h1 style="color:brown">Oops, </h1><p><h3>Something Went Wrong!</h3>   </p><?php }else
               if($x==3)
               {

                ?>
                <h1 style="color:brown">Sorry, </h1><p><h3>It seems that you reloaded the page!</h3> 
              </p><?php }else
              if($x==4)
              {

                ?>
                <h1 style="color:brown">Alert, </h1><p><h3>Suspicious Activity</h3> 
              </p><?php }?>
            </div>

          </div>
        </div><!--/row-->
      </div><!--/span-->


    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="offcanvas.js"></script>
    <script>
      $('.dropdown-toggle').dropdown();
    </script>



  </body>
  </html>

