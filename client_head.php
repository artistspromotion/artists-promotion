<?php
require_once ('include/database.php');
require_once('path.php');
?>

<?php
$userId = $_SESSION['id'];

$q = "SELECT COUNT(*) FROM `notification` WHERE `type`=0 AND `id` NOT IN ( SELECT `notification` FROM `notificationStatus` WHERE `user`=:userId );";
$s = $dbh-> prepare ($q);
$s-> bindParam (':userId', $userId);
$s-> execute();
$r = $s-> fetch(PDO::FETCH_NUM);

$generalNotificationsCount = $r[0];

/* Getting toastr Notifications */
$q = "SELECT * FROM `notification` WHERE `type`=2 AND `id` NOT IN ( SELECT `notification` FROM `notificationStatus` WHERE `user`=:userId );";
$s = $dbh-> prepare ($q);
$s-> bindParam (':userId', $userId);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$unreadToastrNotifications = $r;

$q = "INSERT INTO `notificationStatus` (`notification`, `user`) VALUES (:notificationId, :userId);";
$s = $dbh-> prepare($q);
$s-> bindParam(":userId", $_SESSION['id']);

foreach ($unreadToastrNotifications as $val)
{
  $s-> bindParam(":notificationId", $val['id']);
  $s-> execute();
}
/**/

$q = "SELECT COUNT(*) FROM `message` WHERE `to`=:userId AND `status`=0;";
$s = $dbh-> prepare ($q);
$s-> bindParam (':userId', $userId);
$s-> execute();
$r = $s-> fetchAll();

$messageCount = $r[0][0];
?>

<!DOCTYPE html>
<html lang="en">
<body>
<style>
.unreadNotification
{
  background-color: lightblue;
}



</style>

<link href="toastr/toastr.css" rel="stylesheet">

  <div class="navbar navbar-inverse navbar-fixed-top" style="background:#000f28; height:60px;text-align:center; padding:7px 10px 0px 10px;" role="navigation">
    <div class="container">
      <div class="header">

        <ul class="nav navbar-nav ">
         <li><a href="client_home.php" style="color:rgb(255,255,255); font-family:sans-serif;;font-size:22px;">
          <img src="logo.png"   style="width:50px;height:50px;vertical-align:text-top;">
        <b> <span style="color:green;"> Artist</span> <span style="color:#4169E1;">ePromotion</span></b>

        </a></li> 

		 
		
 <li><a  href="<?php echo $path?>/client_home.php?serviceType=1" style=" color:white;font-size:15px; font-family:sans-serif; text-alig:center">
           HOME </a></li>

		    <li><a href="<?php echo $path?>/client_home.php?serviceType=2" style="color:white; font-size:15px; font-family:sans-serif;"> PROMOTE VIDEO</a></li>
		   
        <li><a  href="<?php echo $path?>/client_home.php?serviceType=1" style=" color:white;font-size:15px; font-family:sans-serif; text-alig:center">
            PROMOTE MUSIC </a></li>
         
          <li><a href="<?php echo $path?>/client_home.php?serviceType=3" style="color:	 white; font-size:15px; font-family:sans-serif;"> BUILD FANBASE</a></li>
            
 </ul>

        <ul class="nav navbar-nav pull-right">
         <li><a href="messages_client.php?serviceId=<?php echo $activeService['id'] ?>"style="color:white; font-family:sans-serif;; font-size:15px; text-alig:center"><span class="glyphicon glyphicon-envelope"></span> <?php if ($messageCount>0) { ?><span class="badge pull-right"><?php echo $messageCount ?></span> <?php } ?>
      </a></li>
          <li>
            <a id="notificationModalTrigger" href="#" rel="popover" data-toggle="modal" data-target="#notificationModal"  style=" color:white; font-family:sans-serif;;  font-size:15px; text-alig:center;">
              <span class="glyphicon glyphicon-flash"></span> <?php if ($generalNotificationsCount>0) { ?><span id="generalNotificationCount" class="badge pull-right"><?php echo $generalNotificationsCount ?></span> <?php } ?>
            
            </a>
          </li>



       <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#DCDCDC;"><span class="glyphicon glyphicon-user"></span> <b><?php echo $_SESSION['user_name'];?></b><b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="#changepassword" data-toggle="modal" >Change Password</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Logout</a></li>
                    </ul>
                </li>




       </ul>      

   </div>
</div>
</div>

<div class="panel panel-default" style="margin-top:-15px;background:green;height:40px;" >
  <div class="panel-body">
<p align="center" style="color:white;"  >  NOTIFICATIONS </p>
  </div>
</div>

   <div class="modal fade" id="changepassword"  style="background:rgba(102,0,204,0.1);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


      <div class="modal-dialog" >
        <div class="modal-content">
          <div class="modal-header" style="background:rgba(0,51,102,1);">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel" style="color:white;">Change Password</h4>
          </div>
          <div class="modal-body" style="background:rgba(0,51,102,.0);">
           <?php
           if(isset($_GET['message']))
           {
            ?>
            <div class="alert alert-success alert-block">
             <a class="close" data-dismiss="alert" href="#">&times;</a>
             <h4 class="alert-heading"></h4>

             <?php
             echo $_GET['message'];
             ?>
           </div>
           <?php
           
         }

         ?>

         
         <!--   Sign up form   -->
         <form  id="changepassword1" role="form" method="POST" class="form-horizontal" enctype="multipart/form-data">
          <div class="form-group" >
           <label for="curpassword" class="col-sm-4 control-label">Current Password</label>
           <div class="col-sm-7">
             <input type="text" class="form-control" id="curpassword" name="cur_password" placeholder="Current Password" required>
           </div>

         </div>

         <div class="form-group">
           <label for="newpassword" class="col-sm-4 control-label">New Password</label>
           <div class="col-sm-7">
             <input type="password" class="form-control" id="newpassword" name="new_password" placeholder="New Password" required>
           </div>

         </div>
         <div class="form-group">
           <label for="repassword" class="col-sm-4 control-label">Re Enter Password</label>
           <div class="col-sm-7">
             <input type="password" class="form-control" id="repassword" name="re_password" placeholder="Re Enter Password" required>
           </div>

         </div>
         
         <hr>
         <div class="form-actions" style="text-align:center;">
           <button type="submit" class="btn btn-primary">Submit</button>
           <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>

         </div>
       </form>
       <div class="modal-footer">
       </div>


     </div> 
   </div><!-- /.modal-content -->
 </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Notification Modal -->
<div class="modal fade" id="notificationModal" style="background:rgba(102,0,204,0.1);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header" style="background-color: rgba(102,0,204,0.1);">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Notifications</h4>
      </div>
      <div class="modal-body" style="background:rgba(0,51,102,.0);">
        <div id="notificationModalLoading">Loading!</div>
        <div id="notifications"></div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

   <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
   <script src="js/bootstrap.js"></script>
   
   <script src="js/bootstrap.min.js"></script>
   <script src="offcanvas.js"></script>

   <script>
    $('.dropdown-toggle').dropdown();
  </script>
<script>  
$(function ()  
{
 $("#message").popover({placement:'bottom', html:"true"});  
});  
</script> 
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
      <script>
        $(function () {
          $('#changepassword1').on('submit', function (e) {
            $.ajax({
              type: 'post',
              url: 'changepassword.php',
              data: $('#changepassword1').serialize(),
              success: function (msg) {
                $('#changepassword1').append(msg);
              }
            });
            e.preventDefault();
          });
        });
      </script>

<script>
$('#notificationModalTrigger').click (fillNotifications);

function fillNotifications()
{
  $('#notifications').empty();

  $.get('ajax/getNotifications.php', function (data) {
    $('#notificationModalLoading').hide();
    var str = '<ul class="list-group">';
    var notifications = JSON.parse(data);
    for (var i in notifications)
    { 
      var unreadClass;
      if (notifications[i]['status'] == 0)
        unreadClass = " unreadNotification";
      else
        unreadClass = "";

      str += '<a href="clientpage.php?productId='+notifications[i]['productId']+'" id="'+notifications[i]['id']+'"class="list-group-item'+unreadClass+'">';
      str += '<span class="pull-right">';
      switch (notifications[i]['type'])
      {
        case '0' : str += '<abbr title="General Notification"><span class="glyphicon glyphicon-list"></span></abbr>'; break;
        case '1' : str += '<abbr title="Top-Bar Notification"><span class="glyphicon glyphicon-chevron-up"></span></abbr>'; break;
        case '2' : str += '<abbr title="Toastr Notification"><span class="glyphicon glyphicon-comment"></span></abbr>'; break;
      }
      str += '</span>';
      str += '<h3>'+notifications[i]['title']+'</h3>';
      str += notifications[i]['text'].replace(/\r\n/g, '<br>');
      str +='</a>';
    }
    str += '</ul>';
    $('#notifications').append(str);
  });
}
</script>

<script src="toastr/toastr.js"></script>
<script>
var unreadToastrNotifications = <?php echo json_encode($unreadToastrNotifications) ?>;

var str;
for (var i in unreadToastrNotifications)
{
  str = "";
  str += '<b>'+unreadToastrNotifications[i]['title']+'</b><br>';
  str += unreadToastrNotifications[i]['text'];

  toastr.options = {
    "closeButton": true,
    "positionClass": "toast-top-right",
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "0",
    "extendedTimeOut": "0",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
  toastr.options.onclick = function () { window.location.href = 'clientpage.php?productId='+unreadToastrNotifications[i]['productId'] };

  toastr.info(str)
}
</script>
</body>
</html>