<!DOCTYPE html>
<html lang="en">
<head>

  <style>
    #image
    {
      opacity:0.4;

    }

  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Client Home</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/offcanvas.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      
    </head>

    <body>
      <?php

      include "admin_head.php";

      ?>
      <div class="container">

        <?php

        include "admin_sidepanel.php";

        ?>


        
        <div class="container">

          <div class="row">
            <div class="col-md-7">


              <form class="form-horizontal" method="post" align="center" action="create_user_write.php">
                <table class="table table-bordered" >
                <th colspan="2"><center>Create User</center></th>

                  <tr><td>User name</td><td><input type="text" name="id" title="Enter User ID"></input></td></tr>
                  <tr><td>Role</td><td>
                    <label class="radio">
                      <input type="radio" name="radio" id="optionsRadios2" value="A">Admin</label>
                      <label class="radio">
                        <input type="radio" name="radio" id="optionsRadios2" value="E">Employee
                      </label>
                    </td>
                  </tr>
                  <tr><td>Email</td><td><input type="text" name="email" title="Enter User ID"></input></td></tr>
                 
                  <tr><td>New Password</td><td><input type="password" name="password" title="Password is case sensitive"></input></td></tr>
                  <tr><td>Re-enter New Password</td><td><input type="password" name="re-password" title="Password is case-sensitive"></input></td></tr>
                  <tr><td colspan="4"><center><input type="submit" class="btn btn-primary" value="submit" name="submit" ></tr>
                </table>
              </form>








            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->





        <hr>

        <footer>
          <p>&copy; Company 2013</p>
        </footer>

      </div><!--/.container-->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      $('.dropdown-toggle').dropdown();
    </script>

    <script>
     $('.aPopover').popover();
   </script>

