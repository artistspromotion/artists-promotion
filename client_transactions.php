<?php
require_once 'session.php';
?>
 <?php
    require_once ('include/database.php');
?>

    <!DOCTYPE html>
    <html lang="en">
    <head>

        <style>
            #image
            {
                opacity:0.4;

            }

        </style>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

        <title>Client Home</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      <script>
        var activeProduct = <?php echo $activeProduct; ?>;
        var items = <?php echo json_encode ($items) ?>;
        var products = <?php echo json_encode($products) ?>;
      </script>
  </head>

  <body  >
    <?php

    include "client_head.php";

    ?>
    <div class="container">

        <?php

        include "client_sidepanel.php";

        ?>


        <div class="col-xs-12 col-sm-9">
            <p class="pull-right visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
            </p>

<div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Transactions</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span11">
                        <!--  -->
                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Transaction Id</th>
                                                <th>Item id</th>
                                                <th>Item Name</th>
                                                <th>Payment Status</th>
                                                <th>Job Status</th>
                                                <th>Percentage Complete</th>
                                                
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $odd=1;
                                            $user_id=$_SESSION['id'];
                                            $stmt=$dbh->prepare("select * from transactions where user_id='$user_id'");
                                            $stmt->execute();
                                            while($r = $stmt->fetch())
                                            {
                                                $item=explode('#',$r['item_id']);
                                                $ite=$item[1];
                                                $stmt1=$dbh->prepare("select * from item where id='$ite'");
                                                $stmt1->execute();
                                                $r1=$stmt1->fetch();
                                                ?>
                                               
                                                <tr class="odd gradeX" onclick="window.location='maintain_course.php?id=<?php echo $r[0];?>'">
                                                    <td><?php echo $r['id'];?></td>
                                                    <td><?php echo $r['txn_id'];?></td>
                                                    <td><?php echo $r['item_id'];?></td>
                                                    <td><?php echo $r1['name'];?></td>
                                                    <td><?php echo $r['payment_status'];?></td>
                                                    <td><?php echo $r['status'];?></td>
                                                    <td><div class="progress progress-striped active">
  <div class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $r['percentage']."%";?>">
    <span class="sr-only"></span>
</div>
</div></td>
                                                    
                                                </tr>
                                               <?php

                                               }
                                               ?>
                                            
                                            
                                        </tbody>
                                    </table>
                              

     </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>


                </div>


                <hr>

                <footer>
                    <p></p>
                </footer>

            </div><!--/.container-->
            </div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="offcanvas.js"></script>
    <script>
        $('.dropdown-toggle').dropdown();
    </script>

    
</script>
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>


<script>
    $('.aPopover').popover();
</script>

</body>
</html>

