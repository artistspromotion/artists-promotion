<?php
require_once ('include/database.php');
?>
<?php
$q = "SELECT `id`, `name` FROM `service`;";
$s = $dbh-> prepare ($q);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$allServices = $r;

if (isset ($_GET['serviceId']))
  $activeServiceId = $_GET['serviceId'];
else
  $activeServiceId = $allServices[0]['id'];

$inactiveServices = array();
foreach ($r as $val)
{
  if ($val['id'] == $activeServiceId)
    $activeService = $val['name'];
  else
    array_push ($inactiveServices, $val);
}


$q = "SELECT `id`, `name` FROM `product` WHERE `serviceId`=:serviceId;";
$s = $dbh-> prepare ($q);
$s-> bindParam (':serviceId', $activeServiceId);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$products = $r;
$activeProduct = $products[0]['id'];

$questionMarks = str_repeat('?, ', count($r)-1) . '?';
$q = "SELECT * FROM `item` WHERE `productType` IN ($questionMarks);";
$s = $dbh-> prepare ($q);
foreach ($products as $k=>$val)
  $s-> bindParam ($k+1, $val['id']);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$items = $r;
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <style>
    #image
    {
      opacity:0.4;

    }

  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Client Home</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/offcanvas.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      <script>
        var activeProduct = <?php echo $activeProduct; ?>;
        var items = <?php echo json_encode ($items) ?>;
        var products = <?php echo json_encode($products) ?>;
      </script>

    </head>

    <body style="background-image:url('img/bg.png');" >
      <?php

      include "admin_head.php";

      ?>
      <div class="container">

        <?php

        include "admin_sidepanel.php";

        ?>


        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

          <div class="header">
            <div class="navbar navbar-right">

              <div class="btn-group">
                <button type="button" class="btn btn-primary btn-primary dropdown-toggle" data-toggle="dropdown">
                  <?php echo $activeService ?> <span class="caret"></span>
                </button>
                <ul id="services" class="dropdown-menu" role="menu">
                  <?php foreach ($inactiveServices as $val) { ?>
                  <li id="service_<?php echo $val['id'] ?>"><a href="#"><?php echo $val['name'] ?></a></li>
                  <?php } ?>
                </ul>
              </div>
            </div >          
          </div>

          <div class="container">

            <div class="row">
              <div class="col-md-7">

                <ul id="products" class="nav nav-tabs" style="background:rgba(0,51,102,.4);">
                  <?php foreach ($products as $k=>$product) { ?>
                  <li <?php echo "id=\"product_".$product['id']."\"" ?><?php if ($k == 0) echo " class=\" active\"" ?>><a href="#"><?php echo "<b style='color:Green;'>".$product['name']."</b>" ?></a></li>

                  <?php } ?>
                
              </div>
            </div><!--/row-->
          </div><!--/span-->

        </div><!--/row-->


 <div class="col-xs-12 col-sm-9">

          <div class="row">
            
            <br>
            <form id="offer" role="form" action="offer_submit.php" method="POST">
              <div class="form-inline" id="itemRows">
                <div class="form-inline" id="rowNum1">
                  <div class="form-group"><input type="text" id="add_name" name="name[]" class="form-control" placeholder="Name of the offer"/> </div>
                  <div class="form-group"><input type="text" id="add_cost" name="cost[]" class="form-control" placeholder="cost of the offer"/> </div>
                  <div class="form-group"><input type="text" id="add_description" name="desc[]" class="form-control" placeholder="description of the offer"/> </div>

                  
                 </div>
               </div>



               <button id="itemsSubmit" type="submit" class="btn btn-default">Submit</button> <div id="sendingIndicator-sending" style="display: none;">Sending</div> <div id="sendingIndicator-sent" style="display: none;">Sent</div>

             </form>

           </div>
         </div><!--/row-->




        <hr>

        <footer>
          <p>&copy; Company 2013</p>
        </footer>

      </div><!--/.container-->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
     <script>
      $('.dropdown-toggle').dropdown();
    </script>

    <script>
      $('#products li').click( function (e) 
      {
        var id = $(this).attr('id');
        id = id.substr(id.indexOf("_") + 1);

        $('li[id^="product_"]').removeClass ('active');
        $('#product_'+id).addClass ('active');

        productId = id;

        $('#items').empty();

        var ct = 0;
        appendString = "";
        for (var i in items)
        {
          if (items[i]['productType'] == productId)
          {
            if (ct%3==0)
              appendString += '<div class="row" style="margin-top: 10px; margin-left: 0px; margin-right: 0px">';

            appendString += '<div id="item_'+items[i]['id']+'" class="col-sm-4 col-lg-3';
            if (ct%3!=0) appendString += ' col-lg-offset-1';
            appendString += '" style="border:2px solid green; ">';
            appendString += '<p>'+items[i]['name']+'</p>';
            var productName;
            for(var j in products) { if (products[j]['id']==productId) productName=products[j]['name']}
              appendString += '<p>'+productName+'</p>';
            appendString += '<h2 id="cost_item_'+items[i]['id']+'">$'+items[i]['cost']+'</h2>';
            appendString += '<p><a class="btn btn-default aPopover" href="#" role="button" data-toggle="popover" data-content="'+items[i]['desc']+'">View details &raquo;</a></p>';
            appendString += '</div>';

            ct ++;

            if ( (ct==items.length) || (ct%3==0) )
            {
              appendString += '</div>';
            }
          }
        }

        $('#items').append(appendString);
        $('#items').hide();
        $('#items').fadeIn();
        $('.aPopover').popover();
        activeProduct = productId;
      });
</script>

<script>
  $('#services li').click( function (e) 
  {
    var id = $(this).attr('id');
    id = id.substr(id.indexOf("_") + 1);

    var oldUrl = window.location.href;

    var newUrl = "add_offer.php?serviceId=" + id;

    window.location.href = newUrl;
  });
</script>

<script>
var selectedItems = [];

$('div[id^="item_"]').click( function() {
	var id = $(this).attr('id');
  id = id.substr(id.indexOf("_") + 1);

  var cost = $('#cost_item_'+id).html();
  cost = cost.substr(1, cost.length);

  index = $.inArray(id, selectedItems);
  if (index == -1) // Not in array
  {
  	selectedItems.push(id);

  	$('#totalCost').html( parseFloat($('#totalCost').html()) + parseFloat(cost) );

  	$(this).css('background-color', 'grey');
  }
  else
  {
  	selectedItems.splice(index, 1);

  	$('#totalCost').html( parseFloat($('#totalCost').html()) - parseFloat(cost) );

  	$(this).css('background-color', 'transparent');
  }
});

$('#payment').submit(function() {
  for (var i in selectedItems)
  {
  	$('#payment').append('<input type="hidden" name="item[]" value="Item #'+selectedItems[i]+'">');
  }
});
</script>

 <script>
 $('.aPopover').popover();
 </script>

 <script>
 $('#itemsSubmit').click (function()
 {
  $('#offer').append ('<input type="hidden" name="productId" value="'+activeProduct+'">');
  return true;
 });
 </script>