<?php
require_once ('../include/database.php');
?>
<?php
$productId = $_POST['productId'];

$data = $_POST['data'];

$q = "INSERT INTO `item` (`productType`, `cost`, `name`, `desc`, `optional`) VALUES (:productType, :cost, :name, :desc, :optional);";
$s = $dbh-> prepare ($q);
$s-> bindParam (':productType', $productId);

foreach ($data as $val)
{
	$s-> bindParam (':cost', $val['cost']);
	$s-> bindParam (':name', $val['name']);
	$s-> bindParam (':desc', $val['desc']);
	$s-> bindParam (':optional', $val['optional']);

	$s-> execute();
}
?>