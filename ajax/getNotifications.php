<?php
require_once ('../session.php');
require_once ('../include/database.php');
?>
<?php
$q = "SELECT * FROM `notification` ORDER BY `time` DESC;";
$s = $dbh-> prepare($q);
$s-> execute();
$r = $s->fetchAll (PDO::FETCH_ASSOC);

$allNotificatins = $r;

$q = "SELECT `notification` FROM `notificationStatus` WHERE `user`=:userId;";
$s = $dbh-> prepare($q);
$s-> bindParam(":userId", $_SESSION['id']);
$s-> execute();
$r = $s->fetchAll (PDO::FETCH_ASSOC);

$readNotifications = [];
foreach ($r as $v)
{
	array_push ($readNotifications, $v['notification']);
}

$q = "INSERT INTO `notificationStatus` (`notification`, `user`) VALUES (:notificationId, :userId);";
$s = $dbh-> prepare($q);
$s-> bindParam(":userId", $_SESSION['id']);

foreach ($allNotificatins as $key=>$val)
{
	if (in_array($allNotificatins[$key]['id'], $readNotifications))
	{
		$allNotificatins[$key]['status'] = 1;
	}
	else
	{
		$allNotificatins[$key]['status'] = 0;
		$s-> bindParam(":notificationId", $allNotificatins[$key]['id']);
		$s-> execute();
	}
}

echo json_encode($allNotificatins);
?>
