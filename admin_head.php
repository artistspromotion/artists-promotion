<!DOCTYPE html>
<html lang="en">
	<body>

		<div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
			<div class="container">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Artist Promotion</a>
				</div>

				<div class="collapse navbar-collapse">

					<ul class="nav navbar-nav">
						<li class="active"><a href="#">Home</a></li>
						<li><a href="#about">About</a></li>
						<li><a href="#contact">Contact</a></li>
						<li><a href="#contact">Add Funds</a></li>
					</ul>

					<ul class="nav navbar-nav navbar-right">
						<li><a href="#">Welcome: &lt;User Name&gt;</a></li>          
					</ul>

				</div><!-- /.nav-collapse -->

			</div><!-- /.container -->
		</div><!-- /.navbar -->




		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="offcanvas.js"></script>

		<script>
		$('.dropdown-toggle').dropdown();
		</script>

	</body>
</html>