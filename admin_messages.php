<?php

	require_once ('include/database.php');
	//include "admin_head.php";
	//include "admin_sidepanel.php";

?>
<?php
$adminUserId = 1; // Was told that admin's user Id would always be 1. ** CAN ALSO TAKE THIS FROM $_SESSION **

/* Getting the read and unread number of messages to admin (with some ordering & grouping that will make work easier later on) */
// $q = "SELECT COUNT(*), `from`, `status`, `user_name` FROM `message`, `users` WHERE `to`=:adminUserId AND `from`=`user_id` GROUP BY `from`, `status` ORDER BY `time` DESC;";
$q_1 = "SELECT `from`, COUNT(*) `unread`, `time` FROM `message` WHERE `to`=:adminUserId AND `status`=0 GROUP BY `from` ORDER BY `time` DESC";
$q_2 = "SELECT `from`, COUNT(*) `read`, `time` FROM `message` WHERE `to`=:adminUserId AND `status`=1 GROUP BY `from` ORDER BY `time` DESC";
$q_left = "SELECT `R_unread`.`from`, `R_users`.`user_name`, `R_read`.`read`, `R_unread`.`unread` FROM ( ($q_1) AS `R_unread` LEFT OUTER JOIN ($q_2) AS `R_read` ON `R_read`.`from`=`R_unread`.`from`), `users` AS `R_users` WHERE `R_read`.`from`=`R_users`.`user_id` OR `R_unread`.`from`=`R_users`.`user_id`";
$q_right = "SELECT `R_read`.`from`, `R_users`.`user_name`, `R_read`.`read`, `R_unread`.`unread` FROM ( ($q_1) AS `R_unread` RIGHT OUTER JOIN ($q_2) AS `R_read` ON `R_read`.`from`=`R_unread`.`from`), `users` AS `R_users` WHERE `R_read`.`from`=`R_users`.`user_id` OR `R_unread`.`from`=`R_users`.`user_id`";
$q = "($q_left) UNION ($q_right);";
$s = $dbh-> prepare ($q);
$s-> bindParam (':adminUserId', $adminUserId);
$s-> execute ();
$r = $s->fetchAll (PDO::FETCH_ASSOC);
/**/

$messageInfo = [];
foreach ($r as $key=>$val)
{
	$messageInfo[$key]['fromId'] = $val['from'];
	$messageInfo[$key]['fromName'] = $val['user_name'];
	$messageInfo[$key]['read'] = ($val['read'] == NULL) ? 0 : $val['read'];
	$messageInfo[$key]['unread'] = ($val['unread'] == NULL) ? 0 : $val['unread'];
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Artist Promotion</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">

		<link href="css/offcanvas.css" rel="stylesheet">
	</head>

	<body>

<?php

	include "admin_head.php";
	include "admin_sidepanel.php";

?>
		

		
			
				
				<div class="col-xs-12 col-sm-9">
					
					<div class="row">

						<table class="table">
							<tr>
								<th>S No.</th><th>User Name</th><th>Messages</th>
							</tr>
							<?php
							$ct = 1;
							foreach ($messageInfo as $val) { ?>
							<tr>
								<td><?php echo $ct++ ?></td>
								<td><?php echo $val['fromName'] ?></td>
								<td><a href="admin_message_reply.php?to=<?php echo $val['fromId'] ?>"><?php echo $val['read']+$val['unread'] ?> messages (<?php echo $val['unread']?> unread)</a></td>
							</tr>
							<?php } ?>
						</table>


					</div><!--/row-->


				</div><!--/row-->

			<hr>

			<footer>
				<p>&copy; Company 2013</p>
			</footer>

		

		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="offcanvas.js"></script>

		<script>
		$('.dropdown-toggle').dropdown();
		</script>

	</body>
</html>