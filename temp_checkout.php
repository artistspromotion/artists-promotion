<?php
require_once 'session.php';
?>
<?php

require_once ('include/database.php');
?>
<?php
$x=$_POST;
$item=$_POST['item'];
?>

<!DOCTYPE html>
<html lang="en">
<head>

	<style>
		#image
		{
			opacity:0.4;

		}

	</style>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

	<title>Client Home</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/offcanvas.css" rel="stylesheet">

	<!-- Just for debugging purposes. Don't actually copy this line! -->
	<!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
          <![endif]-->

          <script>
          	var activeProduct = <?php echo $activeProduct; ?>;
          	var items = <?php echo json_encode ($items) ?>;
          	var products = <?php echo json_encode($products) ?>;
          </script>

      </head>

      <body>
      	<?php

      	include "client_head.php";

      	?>
      	<div class="container">

      		<?php

      		include "client_sidepanel.php";

      		?>


      		<div class="col-xs-12 col-sm-9">
      			<p class="pull-right visible-xs">
      				<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
      			</p>



      			<div class="container">

      				<div class="row">
      					<div class="col-md-9">
      						<div class="progress progress-striped active">
      							<div class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 66%">
      								<span class="sr-only">66% Complete</span>
      								66% Famous
      							</div>
      						</div>
      						<table class="table">
      							<tr><th>#</th><th>Item Id</th>  <th>Name</th> <th>Cost</th>
      								<th></th>
      							</tr>
      							<?php
      							$total=0;
      							$i=0;
      							foreach($item as $a)
      							{



      								$a=explode('#',$a);
      								$id=$a[1];


      								$sql="select * from  item where id='$id'";
      								$stmt=$dbh->prepare($sql);
      								$stmt->execute();
      								$r=$stmt->fetch(PDO::FETCH_ASSOC);
      								$val=$r;

      								$i++;
      								$total+=$val['cost'];
      								?>
      								<tr id="tr_c_<?php echo $id;?>" class="alert alert-danger"> 

      									<td><?php echo $i;?></td>
      									<td><?php echo "item #".$val['id'];?></td>
                        
                        <td><?php echo $val['name'];?></td>
      									<td ><p id="cost_c_<?php echo $id;?>"><?php echo $val['cost'];?></p></td>
      									
      									<td><a onclick="cart(this.id)" id ="c_<?php echo $id;?>" class="close" data-dismiss="alert">&times;</a></td>
      								</tr>




      								<?php
      							}

      							?>
      						</table>     


      						<div class="col-xs-12 col-sm-9">
      							<p class="pull-right visible-xs">
      								<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
      							</p>

      							<div class="header">
      								<div class="navbar navbar-left">
      									<div id="coupon2">
      									</div>
      									<div id="coupon1">

      										<table class="table"><tr><td>
      											<form id="coupon">
      												<input type="text" id="coupon_code" name="coupon_code" placeholder="Coupon Code">
      												<input type="hidden" id="cost1" name="cost" value="<?php echo $total?>">
      											</input>
      											<td><button id="button_coupon" type="submit" name="paypal" class="btn btn-primary btn" >Apply Coupon</td></button>  
      										
      											<p id="message"></p>
      										</form>
      										<tr>
      											<td><h3>Balance <button id="balance" class="btn btn-lg  btn-warning" disabled> <?php echo $total; ?></button>
      											$
      											</h3>
      										</td>
      									</table>
      								</div>
      							</div>
      							<div class="navbar navbar-right">
      								<table class="table"><tr>

      									<th colspan="2" align="center">Payment Options</th></tr><tr><td>
      									<form id="payment" action="payment_med.php" method ="post">
      										<?php
      										foreach($item as $b)
      										{
      											$a=explode('#',$b);
      											$id=$a[1];


      											echo "<input id='input1_c_".$id."'"."type='hidden' name='item[]' value='".$b."'>";


      										}
      										?>
      										<button type="submit" name="paypal" class="btn btn-primary btn-lg">PayPal</button>
      									</form>
      								</td>


      								<td><form id="payment2" action="payment_med_2.php" method ="post">
      									<?php
      									foreach($item as $b)
      									{
      										$a=explode('#',$b);
      										$id=$a[1];


      										echo "<input id='input_c_".$id."'"."type='hidden' name='item[]' value='".$b."'>";
      									}
      									?>

      									<button type="submit" name="check2" class="btn btn-primary btn-lg">2 Check Out</button>  

      								</form>
      							</td>
      							<tr><td colspan="2" align="center"><img src="img/logo/paypal.png" width="150 px">

      							</table>                        </div >          
      						</div>

      					</div><!--/container-->

      				</div><!--/span-->

      			</div><!--/row-->





      			<hr>



      		</div><!--/.container-->
      		</div>
      		</div>



     
     
     
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="offcanvas.js"></script>
        <script>
        	$('.dropdown-toggle').dropdown();
        </script>


<script src="http://code.jquery.com/jquery-1.9.1.js"></script>


<script>
	$(function () {
		$('#coupon').on('submit', function (e) {
			$.ajax({
				type: 'post',
				url: 'check_coupon.php',
				data: $('#coupon').serialize(),
				success: function (msg) {
					//$('#coupon2').html(msg);
					                	$('#message').html('Coupon applied successfully !');

					var x;
					x=$('#balance').html();


					var final_bal=msg;
                    //alert(y[0]);
                   // alert(msg);
                    // var final_bal=parseFloat(y[0])-parseFloat(msg);
                    if(parseFloat(x)!=(parseFloat(msg)))
                    {
                    	$('#balance').html(final_bal);

                    	$('#button_coupon').prop("disabled",true);
                    	var couponid=$('#coupon_code').val();
                    	$('#payment').append('<input type="hidden" name="discount" value="'+couponid+'">');
                    	$('#payment2').append('<input type="hidden" name="discount" value="'+couponid+'">');

                    //alert(couponid);
                }
                else
                	$('#message').html('Invalid Coupon');
                   // alert("Invalid Coupon");
           },
           error: function(){
           	$('#coupon2').html("error");
           }


       });
  e.preventDefault();
});
});
</script>
<script>
	function cart(id1)
	{
            //alert(id1);
            var cost=$('#cost_'+id1).html();
           //  alert(cost);
           $('#input_'+id1).remove();
           $('#input1_'+id1).remove();
           $('#tr_'+id1).remove();

           cost=parseFloat(cost);
             //alert(cost);
             var y;
             y=$('#balance').html();
                    //alert(y);
                    y=parseFloat(y);
                    cost=parseFloat(y)-cost;
                    $('#balance').html(cost);
                    $('#cost1').val(cost);
                }
            </script>




            <script>
            	$('.aPopover').popover();
            </script>

            <?php
            include 'footer.php';

            ?>

        </body>
        </html>

