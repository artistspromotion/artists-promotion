<?php
require_once 'session.php';
?>
<?php
require_once ('include/database.php');
?>
<?php
$userId = $_SESSION['id'];

$q = "SELECT * FROM `message` WHERE `from`=:userId OR `to`=:userId;";
$s = $dbh-> prepare ($q);
$s-> bindParam (':userId', $userId);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);

$messages = $r;
?>
<?php
$q_1 = "UPDATE `message` SET `status`=1 WHERE `id`=:messageId;";
$s_1 = $dbh-> prepare ($q_1);
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <style>
    #image
    {
      opacity:0.4;

    }

  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Messages</title>
  <!-- Css and js for editor-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="editor/src/bootstrap-wysihtml5.css" />


  <!-- Custom styles for this template -->
  <link href="css/offcanvas.css" rel="stylesheet">


      <script>
        var activeProduct = <?php echo $activeProduct; ?>;
        var items = <?php echo json_encode ($items) ?>;
        var products = <?php echo json_encode($products) ?>;
      </script>
    </head>

    <body style="background-image:url('img/bg.png');" >
      <?php

      include "client_head.php";

      ?>
      <div class="container">

        <?php

        include "client_sidepanel.php";

        ?>


        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

          

          <div class="container">

            <div class="row">
              <div class="col-md-7">

                <br>

                <div class="row">
                  <div class="col-sm-4">
                    <b>Admin</b>
                  </div>
                  <div class="col-sm-4 pull-right text-right">
                    <b>Me</b>
                  </div>
                </div>

                <hr>

                <?php
                foreach ($messages as $message)
                {
                  if ($message['from'] == $userId)
                  {
                    ?>
                    <div class="row">
                      <div class="col-sm-offset-1 col-sm-11 text-right">
                        <?php echo $message['message'] ?>
                      </div>
                    </div>
                    <?php
                  }
                  else // from admin
                  {
                    ?>
                    <div class="row">
                      <div class="col-sm-11<?php if ($message['status']==0) { echo " newMessage"; }?>">
                        <?php echo $message['message'] ?>
                        <?php
                        $s_1-> bindParam (':messageId', $message['id']);
                        $s_1-> execute();
                        ?>
                      </div>
                    </div>
                    <?php
                  }
                }
                ?>                

                <br>

                <div class="col-sm-12">

                
                     <form action="submitMessage_client.php" method="POST">
                    <textarea name="message" class="textarea form-control" rows="5" style="width: 100%"></textarea>

                    <br><br>
                    <button class="btn-success btn-lg pull-right" type="submit">Submit</button>
                  </form>
                    </div>

              </div>
            </div><!--/row-->
          </div><!--/span-->

        </div><!--/row-->




        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

          <div class="header">
            <div class="navbar navbar-right">

              <!-- <h3>Balance = 100 $</h3> -->


              <!-- <button type="button" class="btn btn-primary btn-lg">Check Out</button>   -->
            </div >          
          </div>

        </div><!--/container-->
        

      </div><!--/.container-->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--scripts for editor-->
    <script src="editor/lib/js/wysihtml5-0.3.0.js"></script>
    <script src="editor/lib/js/bootstrap.min.js"></script>
    <script src="editor/src/bootstrap3-wysihtml5.js"></script>
    <script>
    $('.textarea').wysihtml5();
</script>

    

    
    <script>
      $('.dropdown-toggle').dropdown();
    </script>

    

<script>
  $('.aPopover').popover();
</script>

</body>

</html>

