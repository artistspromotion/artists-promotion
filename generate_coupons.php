<?php

	require_once ('include/database.php');
	//include "admin_head.php";
	//include "admin_sidepanel.php";

?>



<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Artist Promotion</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">

		<link href="css/offcanvas.css" rel="stylesheet">
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
	</head>

	<body>

<?php

	include "admin_head.php";
	include "admin_sidepanel.php";

?>
		

		
			
				
				<div class="col-xs-12 col-sm-4">
					
					<div class="row">

					<form id="coupon_form1">
					<table class="table table-bordered">
					
					<tr>
					<td>Number of Coupons</td>
					<td><input name="coupon_number"></td>
					<tr><td>Percentage</td>
					<td><input name="percentage"></td>
					<tr><td>Min Limit</td>
					<td><input name="limit"></td>
					<input type="hidden" name="per">
					<tr><td colspan="2"><input type="submit" name="percentage_button" value="Generate Percentage Coupon"></td>
					</tr>
					</table>
					</form>
					<br>
					<table class="table table-bordered">
					<form id="coupon_form2">
					<tr>
					<td>Number of Coupons</td>
					<td><input name="coupon_number1"></td>
					<tr><td>Fixed Amount</td>
					<td><input name="fixed"></td>
					<tr><td>Min Limit</td>
					<td><input name="limit1"></td>
					<input type="hidden" name="fix">
					<tr><Td colspan="2"><input type="submit" name="fixed_amount" value="Fixed Amount"></Td>
					</tr>
					</form>
					</table>
					
					</div>
					</div>
					
					<div class="col-xs-12 col-sm-4">
					
					<div class="row">
					
					<table class="table table-bordered">
					<form id="coupon_form3">
					<tr>
					<td>Static Coupon Code</td>
					<td><input name="static_coupon_code"></td>
					<tr><td>Fixed Amount</td>
					<td><input name="fixed"></td>
					<tr><td>Min Limit</td>
					<td><input name="limit1"></td>
					<tr><td>Date</td>
					<td><input type="date" name="date1"></td>
					<input type="hidden" name="static_fix">
					<tr><Td colspan="2"><input type="submit" name="fixed_amount" value="Static Fixed">
					</tr>
					</form>
					</table>
					<br>

					<table class="table table-bordered">
					<form id="coupon_form4">
					<tr>
					<td>Static Coupon Code</td>
					<td><input name="static_coupon_code"></td>
					<tr><td>Fixed Amount</td>
					<td><input name="fixed"></td>
					<tr><td>Min Limit</td>
					<td><input name="limit1"></td>
					<tr><td>Date</td>
					<td><input type="date" name="date1"></td>
					<input type="hidden" name="static_per">
					<tr><Td colspan="2"><input type="submit" name="fixed_amount" value="Static Percentage">
					</tr>
					</form>
					</table>


					</div><!--/row-->


				</div><!--/row-->

			<hr>

		
		

		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap-datepicker.js"></script>
		<script src="offcanvas.js"></script>
		<script>
        $(function () {
          $('#coupon_form1').on('submit', function (e) {
            $.ajax({
              type: 'post',
              url: 'generate_coupons_backend.php',
              data: $('#coupon_form1').serialize(),
              success: function (msg) {
               alert(msg);
              },
              error: function(){
               
              }

            });
            e.preventDefault();
            });

           $('#coupon_form2').on('submit', function (e) {
            $.ajax({
              type: 'post',
              url: 'generate_coupons_backend.php',
              data: $('#coupon_form2').serialize(),
              success: function (msg) {
               alert(msg);
              },
              error: function(){
                
              }
              

            });
            e.preventDefault();
          });
   
         $('#coupon_form3').on('submit', function (e) {
            $.ajax({
              type: 'post',
              url: 'generate_coupons_backend.php',
              data: $('#coupon_form3').serialize(),
              success: function (msg) {
               alert(msg);
              },
              error: function(){
                
              }
              

            });
            e.preventDefault();
          });
       
 $('#coupon_form4').on('submit', function (e) {
            $.ajax({
              type: 'post',
              url: 'generate_coupons_backend.php',
              data: $('#coupon_form4').serialize(),
              success: function (msg) {
               alert(msg);
              },
              error: function(){
                
              }
              

            });
            e.preventDefault();
          });
        });
      </script>

		<script>
		$('.dropdown-toggle').dropdown();
		</script>

	</body>
</html>