<?php
require_once ('include/database.php');
?>
<?php
$q = "SELECT `id`, `name` FROM `service`;";
$s = $dbh-> prepare ($q);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);
$allServices = $r;

$q = "SELECT `id`, `name`, `serviceId` FROM `product`;";
$s = $dbh-> prepare ($q);
$s-> execute();
$r = $s-> fetchAll(PDO::FETCH_ASSOC);
$allProducts = $r;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Artist Promotion</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">

		<link href="css/offcanvas.css" rel="stylesheet">

		<script>
		var serviceId = <?php echo $allServices[0]['id'] ?>;
		var products = <?php echo json_encode($allProducts) ?>;
		</script>
	</head>

	<body><?php

	include "admin_head.php";
	include "admin_sidepanel.php";

?>
		

	<div class="container">

		<form class="form-horizontal" action="notification_submit.php?id=2" method="POST" enctype="multipart/form-data">
			<fieldset>
			<legend>Notification</legend>

			<div class="form-group">
				<label for="title" class="col-sm-2 control-label">Title : </label>
				<div class="col-sm-10">
					<input id="title" name="title" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="text" class="col-sm-2 control-label">Text : </label>
				<div class="col-sm-10">
					<textarea id="text" name="text" class="form-control" cols="80" rows="10"></textarea>
				</div>
			</div>

			<div class="form-group">
				<label for="image" class="col-sm-2 control-label">Image : </label>
				<div class="col-sm-10">
					<input type="file" id="image" name="image" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Type : </label>
				<div class="col-sm-3">
					<input type="radio" class="form-control" name="type" value="2" checked>Toastr
				</div>
				<div class="col-sm-3">
					<input type="radio" class="form-control" name="type" value="1">Top Bar
				</div>
				<div class="col-sm-4">
					<input type="radio" class="form-control" name="type" value="0">General
				</div>
			</div>

			<div class="form-group">
			<label class="col-sm-2 control-label">Serivce : </label>
				<div class="col-sm-10">
					<select id="services" class="form-control">
						<?php foreach ($allServices as $service) { ?>
						<option value="<?php echo $service['id'] ?>"><?php echo $service['name'] ?></option>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="form-group">
			<label class="col-sm-2 control-label">Product : </label>
				<div class="col-sm-10">
					<select id="products" class="form-control" name="productId">
					<?php foreach ($allProducts as $product) if ($product['serviceId'] == $allServices[0]['id']) { ?>
						<option value="<?php echo $product['id'] ?>"><?php echo $product['name'] ?></option>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit">Submit</button>
				</div>
			</div>

			</fieldset>
		</form>

	</div>

		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>

		<script>
		$('#services').change( function() {
			serviceId = $(this).find(":selected").attr('value');
			
			$('#products').empty();

			for (var i in products)
			{
				if (products[i]['serviceId'] == serviceId)
				{
					$('#products').append('<option value="'+products[i]['id']+'">'+products[i]['name']+'</option>');
				}
			}

		});
		</script>

	</body>
</html>