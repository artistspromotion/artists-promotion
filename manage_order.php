<?php
require_once ('include/database.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <style>
    #image
    {
      opacity:0.4;

    }

  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>Client Home</title>

  <!-- Bootstrap core CSS -->

  <!-- Custom styles for this template -->

        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    


  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      <script>
        var activeProduct = <?php echo $activeProduct; ?>;
        var items = <?php echo json_encode ($items) ?>;
        var products = <?php echo json_encode($products) ?>;
      </script>

    </head>

    <body>
      <?php

      include "admin_head.php";

      ?>
      <div class="container">

        <?php

        include "admin_sidepanel.php";

        ?>


        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>

          <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">All Transactions</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span11">

 <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Transaction Id</th>
                                <th>Item id</th>
                                <th>Item Name</th>
                                <th>Payment Status</th>
                                <th>Order Status</th>
                                <th>Percentage</th>
                                <th>Action</th>


                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $odd=1;
                            $user_id=$_SESSION['id'];
                            $stmt=$dbh->prepare("select * from transactions");
                            $stmt->execute();
                            while($r = $stmt->fetch()){
                                $item=explode('#',$r['item_id']);
                                $ite=$item[1];
                                $stmt1=$dbh->prepare("select * from item where id='$ite'");
                                $stmt1->execute();
                                $r1=$stmt1->fetch();


                                ?>
                                <tr class="gradeX" >
                                    <td><?php echo $r['id'];?></td>
                                    <td><?php echo $r['txn_id'];?></td>
                                    <td><?php echo $r['item_id'];?></td>
                                    <td><?php echo $r1['name'];?></td>
                                    <td><?php echo $r['payment_status'];?></td>
                                    <td><?php echo $r['status'];?></td>
                                    <td><?php echo $r['percentage'];?></td>
                                    <td><button class="btn btn-primary btn-sm" type="button" name="edit" value="Edit or delete" id="modal_call" onclick="modal1(<?php echo $r['id'];?>)">Edit </button></td>
                                </tr>

                                <?php
                            }

                            ?>


                        </tbody>
                    </table>

</div>
</div>
</div>
                


                 
                 
              </div >          
            </div>

          </div><!--/container-->
<!-- update modal -->
          <div class="modal fade" style="background:rgba(102,0,204,0.1);" id="update_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header"style="background:rgba(0,51,102,1);">
                  <button type="button" id="cart_close" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel" style="color:white;">Manage Orders </h4>
                </div>
                <div class="modal-body" style="background:rgba(0,51,102,.0);">
                  <!--   Login form   -->
                 <form  role="form" class="form-horizontal" name="form1" method="POST" action="manageorder_write.php"  id="change_form">


                  </form>

                  <div class="modal-footer">
                   
  </div>


                  </div>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
<!-- update modal end -->

          <script language="javascript">
            function  modal1(a)
            {

$('#change_form').html('<table class="table"><tr><td> Transaction serial number <td> '+a+'<input type="hidden" name="id" value="'+a+'"><tr><td>Select option<td><select name="select_type"><option name="Pending">Pending</option><option name="Complete">Complete</option><option name="Under Progress">Under Progress</option><option name="Refund">Refund</option>    </select> <tr><td>Percentage Complete<td><input type="text" name="percentage"><tr><td colspan="2"><div class="form-group"> <div class="col-sm-offset-2 col-sm-10">      <button type="submit" class="btn btn-success">Update</button></div> ');


$('#update_form').modal('show');


}
</script>
<hr>

<footer>
  <p>&copy; Company 2013</p>
</footer>

</div><!--/.container-->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      $('.dropdown-toggle').dropdown();
    </script>

  
 <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>
        <script src="assets/DT_bootstrap.js"></script>

<script>
 $('.aPopover').popover();
</script>

