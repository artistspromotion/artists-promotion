<?php
require_once 'session.php';
?>
<?php

require_once ('include/database.php');
$id=$_POST['coupon_code'];
$cost=$_POST['cost'];
$sql="select * from coupons where coupon_id=:id and coupon_status='Assigned' and min_amount<=:cost; ";
$stmt=$dbh->prepare($sql);
$stmt->bindParam (':id', $id);
$stmt->bindParam (':cost', $cost);
$stmt->execute();
$r=$stmt->fetch();
if($r)
{
if($r['coupon_type']=="fixed")
echo $cost-$r['coupon_value'];
else
if($r['coupon_type']=="percentage")
echo $cost-($r['coupon_value']*$cost)/100;
else
if($r['coupon_type']=="static_fixed" )
{	
	$today=date('Y-m-d');
	if($today<=$r['expiry'])
	echo $cost-$r['coupon_value'];
}
else
if($r['coupon_type']=="static_percentage" )
{	
	$today=date('Y-m-d');
	if($today<=$r['expiry'])
	echo $cost-($r['coupon_value']*$cost)/100;
}
}
else
echo $cost;

?>
